-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 28, 2022 alle 16:28
-- Versione del server: 10.4.22-MariaDB
-- Versione PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atlas_gate`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `association`
--

CREATE TABLE `association` (
  `id` int(11) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `serial_id` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `surname` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `association`
--

INSERT INTO `association` (`id`, `entity_id`, `serial_id`, `name`, `surname`) VALUES
(1, 1, 'f000001', 'John', 'Doe'),
(2, 2, 'f000002', 'Frank', 'Brown'),
(3, 3, 'f000003', 'Mark', 'Green'),
(4, 4, 'f000004', 'Sarah', 'Green'),
(5, 5, 'f000005', 'Eva', 'Brown'),
(6, 6, 'f000006', 'Mark', 'Raffalo'),
(7, 7, 'f000007', 'Johnny', 'Cage'),
(8, 8, 'f000008', 'Mario', 'Bross'),
(9, 9, 'f000009', 'Luigi', 'Bross'),
(10, 10, 'f000010', 'Dante', 'Alighieri'),
(11, 11, 'f000011', 'Maccio', 'Capatonda'),
(12, 12, 'f000012', 'Walter', 'Ballerina'),
(13, 13, 'f000013', 'Franco', 'Rizzo'),
(14, 14, 'f000014', 'Frank', 'Matano'),
(15, 15, 'f000015', 'Rocco', 'Siffredi'),
(16, 16, 'f000016', 'Sandra', 'Mondaini'),
(17, 17, 'f000017', 'Raimondo', 'Vianello'),
(18, 18, 'f000018', 'Fenix', 'Roger'),
(19, 19, 'f000019', 'Orix', 'Marsh'),
(20, 20, 'f000020', 'Ash', 'Brown'),
(21, 21, 'f000021', 'Gigi', 'Rizzi'),
(22, 22, 'f000022', 'Lea', 'Marelli'),
(23, 23, 'f000023', 'Fabrizio', 'Pizzi'),
(24, 24, 'f000024', 'Filippo', 'Giardi'),
(25, 25, 'f000025', 'Luca', 'Cesari'),
(26, 26, 'f000026', 'Matteo', 'Arrazzol'),
(27, 27, 'f000027', 'Pruno', 'Formattato'),
(28, 28, 'f000028', 'Franco', 'Franchi'),
(29, 29, 'f000029', 'Francesco', 'Sguidieri'),
(30, 30, 'f000030', 'Vincenzo', 'Acaselli');

-- --------------------------------------------------------

--
-- Struttura della tabella `ble`
--

CREATE TABLE `ble` (
  `id` int(11) NOT NULL,
  `direction` varchar(255) NOT NULL,
  `tag_id` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `checked` int(11) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `direction` varchar(10) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `surname` varchar(255) DEFAULT '',
  `timestamp` int(11) NOT NULL,
  `portal_id` int(11) NOT NULL,
  `checked` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `peoplecounter`
--

CREATE TABLE `peoplecounter` (
  `id` int(11) NOT NULL,
  `direction` varchar(255) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `checked` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `serialidble`
--

CREATE TABLE `serialidble` (
  `id` int(11) NOT NULL,
  `mac_address` varchar(255) NOT NULL,
  `serial_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `portal_id` int(11) NOT NULL,
  `ip_pc` varchar(30) NOT NULL,
  `ip_anchor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `settings`
--

INSERT INTO `settings` (`id`, `portal_id`, `ip_pc`, `ip_anchor`) VALUES
(1, 1, '192.168.9.129', '192.168.9.128');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `association`
--
ALTER TABLE `association`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ble`
--
ALTER TABLE `ble`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `peoplecounter`
--
ALTER TABLE `peoplecounter`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `serialidble`
--
ALTER TABLE `serialidble`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `association`
--
ALTER TABLE `association`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT per la tabella `ble`
--
ALTER TABLE `ble`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `peoplecounter`
--
ALTER TABLE `peoplecounter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `serialidble`
--
ALTER TABLE `serialidble`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
