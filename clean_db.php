<?php

if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\globals.php';
}else{
    include_once dirname(__FILE__)."/globals.php";
}


$tables = array(
    0 => "ble",
    1 => "log",
    2 => "peoplecounter"
);

$con = open_db_connection();

//controllo se la connesione con il db è andata a buon fine
if (!check_connection($con, $output)){
    $GLOBALS['main_service'] = FALSE;
    $GLOBALS['pc_service'] = FALSE;
    $GLOBALS['ble_service'] = FALSE;
    if ($GLOBALS['print_log']) json_encode($output);
}

for($i = 0; $i < sizeof($tables); $i++){
    $query = "TRUNCATE TABLE ".$tables[$i];
    db_insert_query($con, $query);
    check_query($con, $query, TRUE);
}