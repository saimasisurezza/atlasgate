<?php
$con = open_db_connection();
$settings = getSettings($con);
$GLOBALS['language'] = $settings['language'];
close_db_connection($con);
?>
<div id="overlay" class="hide"></div>
<div id="loader" class="hide"><img src="font/spinner/loader.svg"/></div>

<div id="header">
    <div class="normalContentWidth">
        <?php
        if(isset($consolle)){
            echo  '<div id="title"><a href="'.$GLOBALS["home_page"].'"></a>'.$GLOBALS["APP_NAME"].'</div>';
        }else{

        ?>
        <div id="title"><a href="<?php echo $GLOBALS['home_page'];?>"><i class="fas fa-home"></i> </a><?php echo $GLOBALS['APP_NAME'];?></div>

        <div id="ico-cont">
            <?php
            /*
            $newTicket = '';
            if($logged){
                if($_SESSION['login_user_welcon']['user_level'] == ADMIN ){
                    $newTicket .= '<a id="manage-users" class="header-ico" href="'.$GLOBALS['manage_user_page'].'" target="_self">';
                    $newTicket .= '<div class="cont-icon"><i class="fas fa-users-cog"></i><span class="header-info">Gestisci utenti</span></div></a>';
                }
            }
           */
            ?>
            <div id="menu" class="header-ico">
                <div class="cont-icon" id="toLog"><i class="fas fa-user"></i><span class="header-info"><?php
                        if ($logged) {
                            echo $_SESSION['login_user_welcon']['name'] . ' ' . $_SESSION['login_user_welcon']['surname'];
                        } else {
                            echo getTextT(0);
                        }
                        ?></span></div>
                <div class="cont-icon hide" id="logged"><i class="fas fa-user"></i><span class="header-info"></span>
                </div>
                <div id="menu-ul" class="header-list hide">
                    <ul>
                        <?php
                        if ($logged) {
                            //echo '<li class="limenu" id="profile-button"><a href="'.$GLOBALS['user_page'].'" target="_self">'.getTextT(1).'</a></li>';
                            echo '<li class="limenu"  id="logout-button">' . getTextT(2) . '</li>';
                        } else {
                            echo '<li class="limenu" id="login-button">' . getTextT(0) . '</li>';
                            //echo '<li class="limenu" id="reg-button">'.getTextT(6).'</li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>

        </div>

        <?php
        }
        ?>

    </div>
</div>

<div id="login-modal" class="modal hide" data-height="350" data-url="<?php  echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>">
    <div class="modaltitle"><?php echo getTextT(0); ?></div>
    <div id="modalCloseLogin" class="closemodal hide"><i class="fas fa-times fa-2x"></i></div>
    <input id="modalName" class="inputlogin" type="email" placeholder="<?php echo getTextT(7); ?>"> <!--value="admin"> -->
    <input id="modalPassword" class="inputlogin" type="password" placeholder="<?php echo getTextT(8); ?>"> <!--value="welcon_admin20"> -->
    <div id="modalLoginButton" class="button"><?php echo getTextT(0); ?></div>
    <div id="forgotPassword" class="textloginlink hide"><?php echo getTextT(3); ?></div>
    <div id="modalResponseError" class="msgKo hide"></div>
    <div id="modalResponseSuccess" class="msgOk hide"></div>
</div>

<div id="forgot-modal" class="modal hide" data-height="300">
    <div class="modaltitle"><?php echo getTextT(4); ?></div>
    <div id="modalCloseForgot" class="closemodal"><i class="fas fa-times fa-2x"></i></div>
    <input class="hide" type="email" placeholder="">
    <input id="modalEmailForgot" class="inputlogin" type="text" placeholder="<?php echo getTextT(7); ?>">
    <div id="modalForgotButton" style="margin-top:30px; width:130px;" class="button"><?php echo getTextT(5); ?></div>
    <div id="modalForgotError" class="msg msgKo hide"></div>
    <div id="modalForgotSuccess" class="msg msgOk hide"></div>
</div>

<div id="signin-form" class="modal hide" data-height="495">

    <div class="modaltitle"><?php echo getTextT(9); ?></div>
    <div id="modalCloseReg" class="closemodal"><i class="fas fa-times fa-2x"></i></div>

    <input style="display:none">
    <input type="password" style="display:none"><!-- hide autocomplete -->
    <input type="text" name="email" style="display:none"><!-- hide autocomplete -->
    <input id="logEmail" class="inputlogin" type="email" placeholder="<?php echo getTextT(10); ?> *" name="email-pre" >
    <input id="logName" class="inputlogin" type="text" placeholder="<?php echo getTextT(11); ?>" name="name-pre">
    <input id="logSurname" class="inputlogin" type="text" placeholder="<?php echo getTextT(12); ?>" name="surname-pre">
    <input id="logPassword" class="inputlogin" type="password" placeholder="<?php echo getTextT(13); ?> *"  name="password-pre">
    <input id="logPassword2" class="inputlogin" type="password" placeholder="<?php echo getTextT(14); ?>"  name="password-pre2">
    <!--<input id="logAge" class="inputlogin" type="number" placeholder="Age" name="age-pre">-->

    <div id="registerButton" class="button"><?php echo getTextT(9); ?></div>
    <div id="responseError" class="msg hide"></div>
    <div id="responseSuccess" class="msg hide"></div>
</div>