<?php
//modelli macchina
define("PRBUS", 0);
define("PR449", 1);

//tipologia funzionamento
define("NORMALCLOSESTD", 0);
define("NORMALOPENSTD", 1);

define("ADMIN", 1);
define("USER", 2);

define("PORTA_APERTA", 1);
define("PORTA_CHIUSA", 2);
define("PORTA_MOVIMENTO", 0);

//COMUNICAZIONE 485
define("IND_PACCHETTO",0);
define("IND_ERRORE",1);
define("IND_INFOERRORE",2);

//ERRORI
define("NESSUN_ERRORE", 0);
define("ERRORE_CONNESSIONE_TTY", 1);
define("ERRORE_MESSAGGIO_VUOTO", 2);
define("ERRORE_CREAZIONE_SOCKET", 3);
define("ERRORE_CONNESSIONE_SOCKET", 4);
define("ERRORE_SCRITTURA_SOCKET", 5);
define("ERRORE_LETTURA_MESSAGGIO", 6);
define("ERRORE_GENERALE_RICHIESTA", 7);
define("ERRORE_GENERICO", 8);

//MACCHINA A STATI
define("QUIETE", 0);
define("APRE_IN", 1);
define("APRE_IN_VUOTO", 2);
define("APRE_OUT", 3);
define("APRE_OUT_VUOTO", 4);
define("ALLARME", 5);
define("EMERGENZA", 6);

define("INIZIALE", 7);
define("APERTO", 8);
define("CHIUSO", 9);

//CODICI EVENTI
define("EV_EMERGENZA_ON", 0);
define("EV_EMERGENZA_OFF", 1);
define("EV_FRODE", 2);
define("EV_ACCESSO_NON_CONSENTITO", 3);
define("EV_LOGIN", 4);
define("EV_LOGOUT", 5);
define("EV_RESET_SISTEMA", 6);

//MODALITà
define('LIBERO', 0);
define('CONTROLLATO',1);
define('BLOCCATO', 2);

//MESSAGGI tipi pacchetto nel socket con python
define("MESSAGGIO", "message");
define("COMANDO", "command");
define("MISURE", "measurement");
define("NOTIFICA", "measurement");


//tempo visualizzazione
define('NORMALTIME', 4);
define('SHORTTIME', 2);
define('LONGTIME', 6);
//colore
define('GREEN', '0,255,0');
define('RED', '255,0,0');
define('BLUE', '0,0,255');
define('YELLOW', '255,255,0');
define('WHITE', '255,255,255');
define('BLACK', '0,0,0');
//fontsize
define('SIZE1', 1);
define('SIZE2', 1.5);
define('SIZE3', 3);
define('SIZE4', 4);
define('SIZE5', 5);
define('SIZE6', 6);
//font family (in info c'è l'immagine OpenCV_font.png con i vari font)
define('FONT1', 'FONT_HERSHEY_SIMPLEX');
define('FONT2', 'FONT_HERSHEY_PLAIN');
define('FONT3', 'FONT_HERSHEY_DUPLEX');// è il default di stampa
define('FONT4', 'FONT_HERSHEY_CPOMPLEX');
define('FONT5', 'FONT_HERSHEY_TRIPLEX');
define('FONT6', 'FONT_HERSHEY_COMPLEX_SMALL');
define('FONT7', 'FONT_HERSHEY_SCRIPT_SIMPLEX');
define('FONT8', 'FONT_HERSHEY_SCRIPT_COMPLEX');
define('FONT9', 'FONT_ITALIC');
//position
define('TOP', 'top');
define('CENTRAL', 'center');
define('BOTTOM', 'bottom');
//comandi
define("CMD_RESTART", "RESTART");
define("CMD_RESTART_THERMAL_CAMERA", "RESTART_THERMAL_CAMERA");
define("CMD_LIGHT_ON", "LIGHT_ON");
define("CMD_LIGHT_OFF", "LIGHT_OFF");
define("CMD_RESTART_FACE_DETECTOR", "RESTART_FACE_DETECTOR");
define("CMD_RESTART_RENDER_LAYOUT", "RESTART_RENDER_LAYOUT");
define("CMD_RESTART_VIDEO_STREAM", "RESTART_VIDEO_STREAM");
