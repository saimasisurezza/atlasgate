<?php

if(PHP_OS =='WINNT'){

    include_once($GLOBALS['engine_path'].'\generalDefine.php');
    include_once($GLOBALS['engine_path'].'\socket_functions.php');
    include_once($GLOBALS['engine_path'].'\dbInteraction.php');
    include_once($GLOBALS['engine_path'].'\bleInteraction.php');
    include_once($GLOBALS['engine_path'].'\gatewayAPI.php');

}else{

    include_once($GLOBALS['engine_path'].'generalDefine.php');
    include_once($GLOBALS['engine_path'].'socket_functions.php');
    include_once($GLOBALS['engine_path'].'dbInteraction.php');
    include_once($GLOBALS['engine_path'].'bleInteraction.php');
    include_once($GLOBALS['engine_path'].'gatewayAPI.php');
    
}

function getSettingsFromDb(){
    $settings = null;
    $con = open_db_connection();

    $query = "SELECT * FROM settings WHERE 1 ORDER BY id DESC LIMIT 1";
    $res = mysqli_query($con, $query);
    if($res){
        $settings =  mysqli_fetch_assoc($res);
    }
    close_db_connection($con);
    return $settings;
}

//UTILS FUNCTION
function compare_events($current_pc, &$ble, $con){
    $result = null;

    for($j = 0; $j < sizeof($ble); $j++){
        if($ble[$j]['direction'] == $current_pc['direction'] &&
                                $ble[$j]['stato'] != ASSOCIATO){

            //$diffTime = $ble[$j]['timestamp'] - $current_pc['timestamp']; 
            $diff_in_seconds = strtotime(date("Y-m-d H:i:s", $ble[$j]['timestamp'])) - strtotime(date("Y-m-d H:i:s", $current_pc['timestamp']));
            if($diff_in_seconds <= $GLOBALS['association_timeout']){
                $result = array(
                    "ble_id" => $ble[$j]['id'],
                    "tag_id" => $ble[$j]['tag_id'],
                    "array_index" => $j
                );
                $ble[$j]['stato'] = ASSOCIATO;

                //aggiorno lo stato nel database a checked, in questo caso infatti sia il ble che il pc non devono essere ripresi
                $query = "UPDATE ble SET checked=1 WHERE id=".$result['ble_id'];
                db_update_query($con, $query);

                $query = "UPDATE peoplecounter SET checked=1 WHERE id=".$current_pc['id'];
                db_update_query($con, $query);
                break;
            }
        }
    }

    return $result;
}


function merge_information($current_array, $result){
    // se è vuoto torno direttamente result
    if (sizeof($current_array) == 0){
        return array_merge($result, $current_array);
    }

    // se non è vuoto faccio un controllo mettendo in un array i nuovi elementi e facendone poi il merge
    $new_element = array();
    foreach ($result as $key => $item) {
        $val =  strpos(json_encode($current_array), $item["id"]) > 0 ? 1 : 0;
        if(!$val){
            $new_element[] = $item;
        }
    }

    return array_merge($current_array, $new_element);
}

function take_cross_identity($con, $tag_id){
    //faccio una richiesta al database association in base al serial_id (tag_ble)
    //prendo nome e cognome e restituisco.
    
    $query = "SELECT name, surname FROM association WHERE serial_id LIKE '".$tag_id."'";
    $result = db_select_query($con, $query);
    if (sizeof($result) > 0){ 
        return array("name" => $result[0]['name'], "surname" => $result[0]['surname']);
    }else{
        //die("ERROR: no person associated with ".$tag_id);
        return array("ERROR" => "No person associated with ".$tag_id);
    }
    //$identity = rand(1, 25);
    //$query = "SELECT name, surname FROM association WHERE entity_id=".$identity;
    //$result = db_select_query($con, $query);
    //return array("name" => $result[0]['name'], "surname" => $result[0]['surname']);
}

function getBLESerialId($result, $mac_address, $con)
{
    // La funzione prende un json in ingresso e un mac address di cui si vuole ricercare il serial id
    // in un primo momento fa la call al db per verificare se al tag con un dato mac address e'giá stato
    // associato il corrispettivo  serial id. Se cosí non fosse allora si ricerca nel json il 'major' relativo
    // al tag con il particolare mac address e poi si aggiunge nel database
    $tag_id = '';

    //controllo sul database se ho gia fatto un associazione mac address --> serial id
    $query = "SELECT serial_id FROM serialidble where mac_address='$mac_address'";
    $out = db_select_query($con, $query);

    if (sizeof($out)){
        return $out[0]['serial_id'];
    }

    // altrimenti ciclo su ogni beacon restituito dall'ancora e controllo prima se c'e' quello relativo al mac address
    // del transito.
    for($i = 0; $i < sizeof($result['beacons']); $i ++){
        if($mac_address == $result['beacons'][$i]['bdaddr']){
            //prendo il major e creo il serial id
            $tag_id = $GLOBALS['head'].$result['beacons'][$i]['ibeacon'][0]['major'];
            // una volta trovato aggiorno anche il database delle associazioni mac address --> serial id
            $query = "INSERT INTO `serialidble` (`mac_address`, `serial_id`) VALUES ('$mac_address', '$tag_id')";
            db_insert_query($con, $query);
            return $tag_id;
        }
    }

    // nel caso non abbia avuto nessuna associazione, lascio come tag_id il mac address...da capire come fare....
    $tag_id = $mac_address;
    return $tag_id;
}




//SESSIONE
function check_login()
{
    //var_dump($_SESSION);
    if (session_id() == '' || !isset($_SESSION)) session_start();
    $logged = false;
    if (isset($_SESSION['login_user_welcon'])) { //controllo per capire se la sessione è stata creata
        $logged = true;
    }

    $now = getTime();
    if (isset($_SESSION['expire'])) {
        if ($now > $_SESSION['expire']) {
            session_destroy();
            $logged = false;
        } else {
            $_SESSION['expire'] = $now + ($GLOBALS['sessiontime'] * 60);
        }
    }

    return $logged;
}

function logout(){
    if(session_id() == '' || !isset($_SESSION))
        session_start();
    if(session_destroy()) {
        header("Location: ".$GLOBALS['home_page']); // Redirecting To Home Page
        exit();
    }
}

//TIME
function getTime(){ //mi ritorna il timestamp relativo alla timezone impostata
    $tz_obj = new DateTimeZone($GLOBALS['timezone']);
    $today = new DateTime("now", $tz_obj);
    return $today->getTimestamp();
}


?>
