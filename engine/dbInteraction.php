<?php
//PULIZIA DB
function clear_table($con, $table){
    $query = "DELETE FROM $table WHERE checked = 1";
    mysqli_query($con, $query);
}

function open_db_connection(){
    $con = mysqli_connect($GLOBALS['hostDB'] , $GLOBALS['userDB'] , $GLOBALS['password'], $GLOBALS["db"]);
    if (!$con)die('Could not connect: too bad' . mysqli_error($con));
    mysqli_set_charset($con, "utf8");
    return $con;
}

function close_db_connection($con){
    mysqli_close($con);
}

function check_connection($con, &$output){
    if(mysqli_error($con)){
        $output['error'] = 1;
        $output['errorstring'] = mysqli_error($con);
        return FALSE;//die(json_encode($output));
    }
    return TRUE;
}

function check_query($con, $query, $log){
    if (mysqli_error($con)) {
        $output['error'] = 2;
        $output['errorstring'] = mysqli_error($con);
        $output['query'] = $query;
        if($log) var_dump(json_encode($output));
    }
}

function insert_with_returned_id($con, $query, $log){
    if(mysqli_query($con, $query)){
        return mysqli_insert_id($con);
    }elseif (mysqli_error($con)) {
        $output['error'] = 2;
        $output['errorstring'] = mysqli_error($con);
        $output['query'] = $query;
        if($log) var_dump(json_encode($output));
        return -1;
    }
}

function db_select_query_state($con, $query, $stato){
    $result = mysqli_query($con, $query);

    $output = array();
    $index = 0;
    while($row = mysqli_fetch_assoc($result)){
        $output[$index] = $row;
        $output[$index]['stato'] = $stato;
        $index ++;
    }

    mysqli_free_result($result);

    return $output;
}


function db_select_query($con, $query){
    $result = mysqli_query($con, $query);

    $output = array();
    $index = 0;
    while($row = mysqli_fetch_assoc($result)){
        $output[$index] = $row;
        $index ++;
    }

    mysqli_free_result($result);

    return $output;
}


function db_insert_query($con, $query){
    $result = mysqli_query($con, $query);

    return $result;
}

function db_update_query($con, $query){
    mysqli_query($con, $query);
    
}

