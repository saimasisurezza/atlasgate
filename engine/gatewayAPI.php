<?php

function getTagsInfo($pageNumber = 0, $pageSize = 5, $log = FALSE){
    // API URL
    $url = $GLOBALS['apiGetTags']."pageNumber=".$pageNumber."&pageSize=".$pageSize;

    // Create a new cURL resource
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPGET, 1);

    // Return response instead of outputting
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Execute the POST request
    $result = curl_exec($ch);

    //decode response and check for some error
    $result = json_decode($result, true);

    // check if there is an error in HTTP post
    if(array_key_exists("error", $result)){
       if ($log) echo "\n\n Status : ".$result["status"]." with Error : ".$result["error"]."\n\n";
       $result = NULL;
    }
    
    // check if the gateway is online
    if(is_null($result)){
        if ($log) echo "\n\n Status Server offline. Check Server or Internet Connection \n\n ";
    }

    // Close cURL resource
    curl_close($ch);

    //result è un array di elementi del tipo {"entityId":int, "serialId": string, "name": string, "surname": string}
    return $result;
}

function pushCrossingInfo($data, $log){

    // Create a new cURL resource
    $ch = curl_init($GLOBALS['apiPushInfo']);
    curl_setopt($ch, CURLOPT_POST, 1);

    // Setup request to send json via POST
    $payload = json_encode($data);

    // Attach encoded JSON string to the POST fields
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

    // Return response instead of outputting
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

    // Execute the POST request
    $result = curl_exec($ch);
    // Close cURL resource
    curl_close($ch);

    //decode response and check for some error
    $result = json_decode($result, true);
    if($result){
        if(array_key_exists('error', $result)){
            if ($log) echo "STATUS : ".$result['status']."; ERROR : ".$result['error'].";<br>".$payload;
            return FALSE;
        }
    }

    return TRUE;

}