<?php


function create_socket_custom($target, $port, $to_send){
    if(!($sock = socket_create(AF_INET, SOCK_DGRAM, 0))) {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);

        die("Couldn't create socket, error: [$errorcode] $errormsg \n");
    }
    //echo "\nSocket created \n";

    if(!$to_send) {
        // Bind the source address solo se devo ricevere il messaggio
        if (!socket_bind($sock, $target, $port)) {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);

            die("Could not bind socket, error: [$errorcode] $errormsg \n");
        }
        //echo "\n\n Socket bind OK \n";
    }

    return $sock;
}

function send_pack($msg, $ip, $port){
    $ret = true;
    $len = strlen(json_encode($msg));
    $sock = create_socket_custom($ip, $port, true);
    //echo "\n sending "; print_r($msg);
    $sendMsg = socket_sendto($sock, $msg, $len, 0, $ip, $port);

    if (!$sendMsg) {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);
        $ret = false;
        die("Could not send message, error: [$errorcode] $errormsg \n");
    }

    socket_close($sock);
    return $ret;
}