# Install java
https://www.linode.com/docs/guides/how-to-install-openjdk-on-ubuntu-20-04/

# start the gateway

java -jar service/atlas.gateway-0.0.1-SNAPSHOT.jar --spring.profiles.active=dev,saima,api-mock

# chimaata di get verso 

http://localhost:8081/api/tags?pageNumbber=0&pageSize=5

# chimata di push in cui passo un array JSON

[
    {
        obj 1
    },

    {
        obj 2
    }
]

http://localhost:8081/api/publish