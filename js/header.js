(function ($) {
    $.header = function () {
        this.printLog = true;
    };

    $.header.prototype = {
        init: function (pl) {
            var _self = this;
            _self.printLog = pl;

            //MENU inizio
            $('#menu').on('mouseenter', function(){
                $('#menu-ul').removeClass('hide');
            });

            $('#menu').on('mouseleave', function(){
                $('#menu-ul').addClass('hide');
            });

            //MENU fine
/*
            //FORM CREATE USER IN
            _self.handleWindow('reg-button', 'signin-form', 'modalCloseReg', false);    
            $("#registerButton").on('click',function(e) {
                _self.createUser();
            });
*/
            //LOGIN
            _self.handleWindow('login-button', 'login-modal', 'modalCloseLogin', false);
            $('#modalLoginButton').on('click', function () {
                _self.checkLogin();
            });

            $('#modalName, #modalPassword').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    _self.checkLogin();
                }
            });
/*
            //FORGOT PASSWORD MODAL
            _self.handleWindow('forgotPassword', 'forgot-modal', 'modalCloseForgot', true);
            $('#modalForgotButton').on('click', function () {
                _self.resetPassword();
            });
*/
            $('#logout-button').on('click', function(){
               _self.logout();
            });

            $('#modalPassword').keypress(function(event){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    _self.checkLogin();
                }
            });

        }, //FINE INIT

        createUser:function(){
            var _self = this;
            if($(this).hasClass('disabled')) return;

            $(this).addClass('disabled');

            if($('#logEmail').val() === "") {
                $('#logEmail').addClass('error');
                return;
            }
            if (!_self.checkEmail($('#logEmail').val())) {
                $('#logEmail').addClass('error');
                return;
            } else $('#logEmail').removeClass('error');

            if( $('#logPassword').val() === "") {
                $('#logPassword').addClass('error');
                return;
            }else  $('#logPassword').removeClass('error');

            if( $('#logPassword').val() != $('#logPassword2').val()) {
                $('#logPassword, #logPassword2').addClass('error');
                return;
            }else{
                $('#logPassword, #logPassword2').removeClass('error');
            }

            if($( "#office" ).val() == null){
                $('#office').addClass('error');
                return;
            }

            var name = $('#logName').val();
            var surname = $('#logSurname').val();
            var password =  $('#logPassword').val();
            var email = $('#logEmail').val();
            var office = $( "#office" ).val();

            $('#loader').removeClass('hide');

            _self.hideMessageForm('responseError');

            $.ajax({
                type: "POST",
                url: "api/createUser.php",
                dataType: "json",
                data: {email: email, password:password, name:name, surname:surname, office:office},
                success: function(response) {
                    console.log(response);

                    if(response.error == 0) {
                        $('#responseSuccess').text(response.errorstring);
                        $('#responseSuccess').removeClass('hide');
                        $('#loader').addClass('hide');

                        _self.waitBeforeClose('signin-form'); //chiude il form e toglie l'overlay dopo un tot di secondi
                    }else{
                        _self.showMessageForm('responseError', response.errorstring);
                    }

                    $(this).removeClass('disabled');
                },
                error: function(e) {
                    console.log(e);
                    $(this).removeClass('disabled');
                }
            });
        },
        checkLogin: function(){
            var _self = this;
            var email = $('#modalName').val();
            var password = $('#modalPassword').val();
            console.log(_self.checkEmail(email));
            /*
            if(email == "" || !_self.checkEmail(email)){
                $('#modalName').addClass('error');
                return;
            }
            */
            if(email == ""){
                $('#modalName').addClass('error');
                return;
            }
            $('#modalName').removeClass('error');
            if(password == ""){
                $('#modalPassword').addClass('error');
                return;
            }
            $('#modalPassword').removeClass('error');

            $('#modalResponseError').addClass('hide');

            $('#loader').removeClass('hide');
            var url = $('#login-modal').attr('data-url');

            $.ajax({
                type: "POST",
                url: 'api/login.php',
                dataType: "json",
                data: {user: email, password:password, url:url},
                success: function (response) {
                    console.log(response);
                    if(response.error != 0){
                        _self.showMessageForm('modalResponseError', response.errorstring);
                    }else{
                        window.location = response.url;
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        },
        logout:function(){
            var url = $('#login-modal').attr('data-url');

            $.ajax({
                type: "POST",
                url: 'api/logout.php',
                dataType: "json",
                data: {url:url},
                success: function (response) {
                    console.log(response);
                    window.location = response.url;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        },

        handleWindow: function(openbutton, modal, close, isForgot) {

            $('#'+openbutton).on('click', function () {

                var width = Math.round(($(window).innerWidth() - $('#'+modal).width()) / 2);
                var height = Math.round(($(window).innerHeight() - $('#'+modal).attr('data-height')) / 2);

                $('#'+modal).css({left: width + 'px', top: height + 'px'});

                if ($('#'+modal).hasClass('hide')) {
                    if(!$('#overlay').hasClass('hide') && !isForgot) return;
                    if(isForgot) $('#login-modal').addClass('hide');
                    $('#'+modal+', #overlay').removeClass('hide');
                } else {
                    $('#'+modal+', #overlay').addClass('hide');
                }
            });

            $('#'+close).on('click', function () {
                $('#'+modal+', #overlay').addClass('hide');
            });
        },

        resetPassword: function(){
            var _self = this;
            var email = $('#modalEmailForgot').val();
            console.log(_self.checkEmail(email));
            if(email == "" || !_self.checkEmail(email)){
                $('#modalEmailForgot').addClass('error');
                return;
            }
            $('#modalEmailForgot').removeClass('error');
            $('#modalForgotError').addClass('hide');

            $('#loader').removeClass('hide');

            $.ajax({
                type: "POST",
                url: 'api/resetPassword.php',
                dataType: "json",
                data: {email: email},
                success: function (response) {
                    console.log(response);
                    if(response.error != 0){
                        _self.showMessageForm('modalForgotError', response.errorstring);
                    }else{
                        _self.showMessageForm('modalForgotSuccess', response.errorstring);
                        _self.waitBeforeClose('forgot-modal');
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });

        },

        waitBeforeClose:function(div){
            var _self = this;
            setTimeout(function(){
                $('#'+div).addClass('hide');
                $('#'+div+' .msg').text('').addClass('hide');
                $('#'+div+' input').val('');
                $('#overlay').addClass('hide');
            }, 3000);
        },
        showMessageForm:function(div, text){
            $('#loader').addClass('hide');
            $('#'+div).text(text);
            $('#'+div).removeClass('hide');
        },

        hideMessageForm:function(div){
            $('#'+div).text('');
            $('#'+div).addClass('hide');
        },
        checkEmail:function(email) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            return reg.test(email);
        },


    };

    $.header.defaults = {
    };

}(jQuery));

