(function ($) {
    $.main = function () {
        //dichiarazione variabili alle quali si accede con _self.
        //this.isEmergency = false;
        this.BUTTON_PRESSED = 0x01;
        this.MOVEMENT = 0x04;
        this.tag = [];
        this.iterError = 0;
        this.numMaxError = 5;

        this.pollingTime = 1000;
    };

    $.main.prototype = {
        init: function () {
            var _self = this;
            /*
          _self.interval = setInterval(() => _self.checkTagStatus() , 10000);
          setTimeout(function() {
              _self.pollingStato();
          },1000);
          */
           // _self.getId();
            //self.checkTagStatus();
            //_self.interval = setInterval(() => _self.checkTagStatus() , _self.pollingTimer);

            //_self.checkTagStatus();

            _self.pollingStatus();
        },


        checkTagStatus: function(){
            var _self = this;
            //console.log('checkMessageStarted');
            $.ajax({
                type: "GET",
                url: './service/pollingBle.php',
                dataType: "json",
                //data: {cmd: cmd},
                success: function (response) {
                    console.log(response);

                    for(var l=0; l<_self.tag.length; l++){
                        _self.tag[l].found = false;
                    }

                    if(response.result == "success") {
                        $('#erroreBlue').remove();
                        _self.iterError = 0;

                        console.log("Tag rilevati: "+response.beacons.length);

                        for (var i = 0; i < response.beacons.length; i++) {

                            for(var k=0; k<_self.tag.length; k++){
                                if(response.beacons[i].serial == _self.tag[k].id) {
                                    _self.tag[k].found = true;
                                }
                            }

                            var maxIndex = 0;
                            var ultimoTs = new Date(response.beacons[i].ibeacon[maxIndex].timestamp);
                          //  console.log("lunghezza ibeacon "+response.beacons[i].ibeacon.length+" ovvero \"eventi\" per ogni tamper");

                            for(var j=1; j<response.beacons[i].ibeacon.length; j++) {
                                var attualeTs=  new Date(response.beacons[i].ibeacon[j].timestamp);
                                //console.log(attualeTs+' vs '+ultimoTs);
                                //console.log("ultimo: "+response.beacons[i].ibeacon[maxIndex].timestamp+" ||| attuale: "+response.beacons[i].ibeacon[j].timestamp);
                                if(ultimoTs.getTime() < attualeTs.getTime()) {
                                    //console.log("switch");
                                    ultimoTs = attualeTs;
                                    maxIndex = j;
                                }else{
                                //    console.log("il campione sono sempre io");
                                }
                            }
                            //console.log('scelto '+ maxIndex);

                            var currentMinor = response.beacons[i].ibeacon[maxIndex].minor;
                            var batteria = currentMinor >> 8;
                            batteria = (batteria*10) + 1700;
                            batteria = Math.ceil((batteria*100)/3000);
                            var flags = currentMinor & 0xff;

                            if(response.beacons[i].serial == 7089) {
                                //console.log(response.beacons[i]);
                                //console.log(batteria);
                            }

                            _self.createSquare(flags & _self.BUTTON_PRESSED, flags & _self.MOVEMENT, batteria, response.beacons[i].serial);
                        }

                        for(var ind=0; ind<_self.tag.length; ind++){
                          if(!_self.tag[ind].found){
                              console.log("TAG "+_self.tag[ind]+" SCOMPARSO");
                              $('.square[data-id="'+_self.tag[ind].id+'"]').addClass('error');
                          }else{
                              $('.square[data-id="'+_self.tag[ind].id+'"]').removeClass('error');
                          }
                        }
                    }
                },
                error: function (error) {
                    if(_self.iterError > _self.numMaxError) {
                        if ($('#erroreBlue').length == 0)
                            $('#main-container').prepend('<div id="erroreBlue">Gateway Bluetooth disconnesso</div>');
                        _self.iterError = 0;
                    }else{
                        _self.iterError++;
                    }
                    //console.log(error);
                }
            });
        },

        pollingStatus: function (){
            var _self = this;

            $.ajax({
                type: "POST",
                url: './service/pollingStatus.php',
                dataType: "json",
                data: {
                    device_id:''
                },
                success: function (response) {
                    //console.log(response);

                    if (response.errore) {
                        console.log('error on connection '+response.errore);
                        return;
                    }
                    
                    
                    if(response.noIterFound != true ) {
                        var el;
                        //console.log(response.associazioni);
                        
                        for (var i = 0; i < response.associazioni.length; i++){
                            var direction = response.associazioni[i].direction;
                            var id_portale = response.associazioni[i].portal_id;
                            var data = _self.convertTimestamp(response.associazioni[i].timestamp);
                            var name = response.associazioni[i].name;
                            var surname = response.associazioni[i].surname;
                            
                            console.log(name);
                            if(name == "" || surname == ""){
                                el = "<li class='event error'>ALLARME > Transito "+direction+" portal id:" + id_portale + " data:" + data + "</li>";
                            }

                            if(name != "" & surname != ""){
                                el = "<li class='event "+direction+"'>Transito di "+name+" "+surname+" :     "+direction+" OK portal id:" + id_portale + " data:" + data + " dir:"+direction+"</li>";
                            }

                            $('#listCont').append(el);
                            $('body').scrollTop($('#listCont').height());

                        }
                    }
                    


                },
                error: function (error) {
                    console.log(error);
                }
            });

            setTimeout(function(){
                _self.pollingStatus()
            }, _self.pollingTime);
        },

        convertTimestamp: function(UNIX_timestamp){
            var a = new Date(UNIX_timestamp * 1000);
            var year = a.getFullYear();
            var month = a.getMonth();
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec ;
            return time;
        },

        sleep: function(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e9; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                    break;
                }
            }
        },

    
    };

    $.main.default = {
        type: 'main'
    };

}(jQuery));
