<?php

if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$output = array();
$output['error'] = NESSUN_ERRORE;

$con = open_db_connection();
$output['data'] = getSettings($con);

close_db_connection($con);
echo json_encode($output);
