<?php
if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$con = open_db_connection();
$settings = getSettings($con);
close_db_connection($con);

if(intval($settings['is_slave'])){
    $con = open_db_connection_master($settings['ip_master']);
}else{
   $con = open_db_connection();
}
if(!$con)
    echo json_encode([getTextT(73)]);
else{
    $query = "DELETE FROM counter WHERE 1";
    mysqli_query($con, $query);

    close_db_connection($con);

    echo json_encode([getTextT(39)]);
}