<?php
if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$output = array();
$output['error'] = NESSUN_ERRORE;

$con = open_db_connection();

$query = "SELECT * FROM status ORDER BY id DESC LIMIT 1";
$result = mysqli_query($con, $query);

if($result && mysqli_num_rows($result)>0){
    $output['status'] = mysqli_fetch_assoc($result);
    $output['status']['door_status'] = get_door_status_text($output['status']['door_status']);
}else{
    $output['error'] = ERRORE_GENERICO;
    $output['text'] = getTextT(58);
}

$settings = getSettings($con);
if(intval($settings['is_slave'])){
    $con = open_db_connection_master($settings['ip_master']);
}

$output['status']['main'] = 1;
$output['status']['pc'] = 1;
$output['status']['model'] = $settings['model'];
if($con){
    $query = "SELECT * FROM counter ORDER BY id DESC LIMIT 1";
    $result = mysqli_query($con, $query);
    if(!$result){
        $output['counters']['error'] = ERRORE_GENERICO;
        $output['counters']['text'] = mysqli_error($con);
    }else {
        if(mysqli_num_rows($result)>0){
            $tmp = mysqli_fetch_assoc($result);
            $output['counters']['entered'] = $tmp['entered'];
            $output['counters']['num'] = $tmp['num'];
        }else{
            $output['counters']['entered'] = 0;
            $output['counters']['num'] = 0;
        }
        $output['counters']['error'] = NESSUN_ERRORE;
        }
} else
    $output['counters']['error'] = ERRORE_GENERICO;

exec("ps aux | grep main.php | awk '{print $2}'", $out1, $ret);
if($ret==0 && sizeof($out1)>2)$output['status']['main'] = 0;
exec("ps aux | grep pollingPc.php | awk '{print $2}'", $out2, $ret);
if($ret==0 && sizeof($out2)>2)$output['status']['pc'] = 0;

close_db_connection($con);

echo json_encode($output);
