<?php
//TEST DI COMUNICAZIONE CON IL PEOPLE COUNTER

if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

$con = open_db_connection();
$settings = getSettings($con);
close_db_connection($con);

$out = array();
$out['error'] = NESSUN_ERRORE;
$out['text'] = getTextT(67);

$header = "GET /HTTP/1.1\r\n" .
    "Origin: Http : //".$settings['ip_pc']."\r\n" .
    "Authorization: Basic cm9vdDpzNGltYXNpYw==\r\n" .
    //"Sec-WebSocket-Protocol: event-protocol\r\n" .
    "Sec-WebSocket-Key: cm9vdDpzNGltYXNpYw==\r\n" .
    "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko\r\n" .
    "Host: ".$settings['ip_pc'].":".$settings['port_pc']."\r\n" .
    "Upgrade: Websocket\r\n" .
    "Sec-websocket-version: 13\r\n" .
    "DNT: 1\r\n" .
    "Cache-Control: no-cache\r\n\r\n";
$msg = "event-protocol";
$buffer = $header." ".$msg;
//$buffer = $header;

$client = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
# connect to our target host
if(!$client){
    //echo socket_strerror(socket_last_error());
    $out['error'] = ERRORE_CREAZIONE_SOCKET;
    $out['text'] = getTextT(64);
}else {
    //echo '<br>creation  OK<br>';

    $connect_timeval = array("sec" => 0, "usec" => $GLOBALS['TIMEOUT_POLLING']);
    socket_set_option($client, SOL_SOCKET, SO_RCVTIMEO, $connect_timeval);
    //il timeout sulla connessione non funziona, va solo sui messaggi successivi quando la connessione è già attiva
    //per avere il timeout anche sulla connessione al socket setto il socket come non bloccante aspetto il tempo di polling e lo risetto a bloccante e controllo
    //se ci sono eventuali errori, se ci sono non ho connssione altrimenti la connessione c'è
    //socket_set_nonblock($client);
    //@socket_connect($client, $target, $port);
    //la @ non mi fa stampare le eventuali warning e mi stampo solo se voglio l'erroe con socket_last_error()

    // switch to non-blocking
    socket_set_nonblock($client);

    // store the current time
    $time = microtime(true);
    // loop until a connection is gained or timeout reached
    while (!@socket_connect($client, $settings['ip_pc'], $settings['port_pc'])) {
        $err = socket_last_error($client);
        // success!
        if ($err === 56 || $err == 0) break;

        $diff = (microtime(true) - $time);
        $code = socket_last_error($client); //echo ' - '.$err.': '.socket_strerror($err).' | '.$code.' --- ';
        if ($diff >= $GLOBALS['TIMEOUT_SOCKET_CONNECTION']) break;
        if (intval($code) == $GLOBALS['SOCKET_CONNECTION_OK']) break;

        socket_clear_error($client);
        // sleep for a bit
        usleep(1000);
    }

    // re-block the socket if needed
    socket_set_block($client);

    if (socket_last_error($client) != 0 && socket_last_error($client) != $GLOBALS['SOCKET_CONNECTION_OK']) {
        socket_close($client);
        $out['error'] = ERRORE_CONNESSIONE_SOCKET;
        $out['text'] = getTextT(65);
    }else {
        // echo 'connection OK<br>';

        if (!socket_write($client, $buffer)) { //quando mando il pacchetto questo deve essere in tale formato: o con 0x00 o direttamente il numero senza lo 0 davanti
            socket_close($client);
            $out['error'] = ERRORE_SCRITTURA_SOCKET;
            $out['text'] = getTextT(66);

        }//else echo 'sending OK<br><br>';

        socket_close($client);
    }
}

echo json_encode($out);

