<?php
if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$con = open_db_connection();
$settings=getSettings($con);

$paramP1 = $paramP2 = $paramED = 0;

if($settings['model'] == PR449) {
    $paramED = $GLOBALS['defaultParamED449'];
}else if($settings['model'] == PRBUS) {
    $paramP1 = $GLOBALS['defaultParamP1'];
    $paramP2 = $GLOBALS['defaultParamP2'];
    $paramED = $GLOBALS['defaultParamED'];
}

$output = writeParam($paramP1, $paramP2, $paramED);

close_db_connection($con);

echo json_encode($output);

