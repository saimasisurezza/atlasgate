<?php
include_once dirname(__FILE__)."/../globals.php";

$GLOBALS['print_log'] = false;

$con = open_db_connection();
$settings = getSettings($con);
clear_table($con, 'peoplecounting');
close_db_connection($con);

$out = openSocket($settings);

function openSocket($settings){
    /*
    la funzione ritorna un array con primo elemento il pacchetto ricevuto, come secondo un errore, come terzo un eventuale stringa di errore
    */

    $res = array();
    $res[IND_PACCHETTO] = "";
    $res[IND_ERRORE] = NESSUN_ERRORE;
    $res[IND_INFOERRORE] = "";

    $header = "GET /HTTP/1.1\r\n" .
        "Origin: Http : //".$settings['ip_pc']."\r\n" .
        "Authorization: Basic cm9vdDpzNGltYXNpYw==\r\n" .
        //"Sec-WebSocket-Protocol: event-protocol\r\n" .
        "Sec-WebSocket-Key: cm9vdDpzNGltYXNpYw==\r\n" .
        "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko\r\n" .
        "Host: ".$settings['ip_pc'].":".$settings['port_pc']."\r\n" .
        "Upgrade: Websocket\r\n" .
        "Sec-websocket-version: 13\r\n" .
        "DNT: 1\r\n" .
        "Cache-Control: no-cache\r\n\r\n";
    $msg = "event-protocol";
    $buffer = $header." ".$msg;
    //$buffer = $header;

    $client = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    # connect to our target host
    if(!$client){
        //echo socket_strerror(socket_last_error());
        $res[IND_ERRORE] = ERRORE_CREAZIONE_SOCKET;
        return $res;
    }
/*
    else{
        echo '<br>creation  OK<br>';
    }
*/

    $connect_timeval = array("sec"=>0, "usec" => $GLOBALS['TIMEOUT_POLLING']);
    socket_set_option($client, SOL_SOCKET, SO_RCVTIMEO, $connect_timeval);
    //il timeout sulla connessione non funziona, va solo sui messaggi successivi quando la connessione è già attiva
    //per avere il timeout anche sulla connessione al socket setto il socket come non bloccante aspetto il tempo di polling e lo risetto a bloccante e controllo
    //se ci sono eventuali errori, se ci sono non ho connssione altrimenti la connessione c'è
    //socket_set_nonblock($client);
    //@socket_connect($client, $target, $port);

    //la @ non mi fa stampare le eventuali warning e mi stampo solo se voglio l'erroe con socket_last_error()

    // switch to non-blocking
    socket_set_nonblock($client);

    // store the current time
    $time = microtime(true);
    // loop until a connection is gained or timeout reached
    while (!@socket_connect($client, $settings['ip_pc'], $settings['port_pc'])) {
        $err = socket_last_error($client);
        // success!
        if ($err === 56 || $err == 0) break;

        $diff = (microtime(true) - $time);
        $code = socket_last_error($client); //echo ' - '.$err.': '.socket_strerror($err).' | '.$code.' --- ';
        if ($diff >= $GLOBALS['TIMEOUT_SOCKET_CONNECTION']) break;
        if (intval($code) == $GLOBALS['SOCKET_CONNECTION_OK']) break;

        socket_clear_error ($client);
        // sleep for a bit
        usleep(1000);
    }

    // re-block the socket if needed
    socket_set_block($client);

    if(socket_last_error($client) != 0 && socket_last_error($client) != $GLOBALS['SOCKET_CONNECTION_OK']){
        socket_close($client);
        $res[IND_ERRORE] = ERRORE_CONNESSIONE_SOCKET;
        return $res;
    }/*else{
        echo 'connection OK<br>';
    }*/

    if(!socket_write($client, $buffer)){ //quando mando il pacchetto questo deve essere in tale formato: o con 0x00 o direttamente il numero senza lo 0 davanti
        socket_close($client);
        $res[IND_ERRORE] = ERRORE_SCRITTURA_SOCKET;
        return $res;
    }/*
    else{
        echo 'sending OK<br><br>';
    }*/

    //$iter=0;
    //$recv_len = 0;

    $response = "";

    //importante si blocca su socket_read se mi metto in ascolto e non mi arriva nulla,
    //per evitare questo devo sapere la lunghezza del messaggio di risposta.
    //per il messaggio di status è 17
    $msg_len = $GLOBALS['MAX_PACK_LENGTH'];

    $zone_number_in = -1;
    $zone_number_out = -1;

    //while ($recv_len<$msg_len) {
    while(TRUE){
        $data = @socket_read($client, $msg_len);
        if($data) {
            $data = cleanCharacter($data);
            if($data != "") {
                $data = json_decode($data, true);
                if(isset($data[0]['Tag'])) {
                    if ($data[0]['Tag'] == $GLOBALS['tagString']) {
                        $ruleType = $data[0]['Data'][0]['RuleType'];
                        $con = open_db_connection();
                        switch ($ruleType){
                            case $GLOBALS['pc_type_count']:
                                $in_num = intval($data[0]['Data'][0]['CountingInfo'][0]['In']);
                                $out_num = intval($data[0]['Data'][0]['CountingInfo'][0]['Out']);
                                debug_log_2("\nIN: ".$in_num."  OUT: ".$out_num." \n");
                                $time = getTime();
                                $portal_id = $settings['portal_id'];
                                $query = "INSERT INTO pc_events (timestamp, portal_id, num_zone_in, num_zone_out, type, num_in, num_out) VALUES ($time, $portal_id, 0, 0, '".$GLOBALS['pc_type_count']."', $in_num, $out_num)";
                                $result = mysqli_query($con, $query);
                                if(!$result){
                                    echo mysqli_error($con);
                                }

                                $con_counter = $con;
                                if(intval($settings['is_slave'])){
                                    $con_counter = open_db_connection_master($settings['ip_master']);
                                    if(!$con_counter) die('Could not connect: too bad' . mysqli_error($con_counter));
                                }
                                controlMaxRows('counter', $con_counter); //controllo che la tabella counter non esploda
                                //UPDATE del numero di utenti all'interno dell'edificio
                                $query = "SELECT * FROM counter ORDER BY id DESC LIMIT 1";
                                $result = mysqli_query($con_counter, $query);
                                if(!$result){
                                    echo mysqli_error($con_counter);
                                }else {
                                    $last_row = mysqli_fetch_assoc($result);
                                    $prev_num = 0;
                                    if (mysqli_num_rows($result) > 0) $prev_num = intval($last_row['num']);
                                    $current_num = $prev_num + $in_num - $out_num;
                                    if($current_num<0) $current_num = 0;

                                    $prev_entered = 0;
                                    if (mysqli_num_rows($result) > 0) $prev_entered = intval($last_row['entered']);
                                    $current_entered = $prev_entered + $in_num;
                                    if($current_entered<0) $current_entered = 0;

                                    $query = "INSERT INTO counter (timestamp, portal_id, num, entered) VALUES ($time, $portal_id, $current_num, $current_entered)";
                                    mysqli_query($con_counter, $query);
                                    if (!$result) {
                                        echo mysqli_error($con_counter);
                                    }
                                }
                                if(intval($settings['is_slave'])){
                                    close_db_connection($con_counter);
                                }

                                break;
                            case $GLOBALS['pc_type_zone']:
                                $zone_in = 0; $zone_out = 0;
                                if($data[0]['Data'][0]['ZoneInfo'][0]['RuleName'] == $GLOBALS['pc_area_in'])
                                    $zone_in = $data[0]['Data'][0]['ZoneInfo'][0]['Inside'];

                                if($data[0]['Data'][0]['ZoneInfo'][0]['RuleName'] == $GLOBALS['pc_area_out'])
                                    $zone_out = $data[0]['Data'][0]['ZoneInfo'][0]['Inside'];

                                if($zone_number_in != $zone_in || $zone_number_out != $zone_out) {
                                    debug_log_2("\nZONE IN: " . $zone_in . " ZONE OUT: " . $zone_out . " \n");
                                }
                                $time = getTime();
                                $portal_id = $settings['portal_id'];
                                $query = "INSERT INTO pc_events (timestamp, portal_id, num_zone_in, num_zone_out, type, num_in, num_out) VALUES ($time, $portal_id, $zone_in, $zone_out, '".$GLOBALS['pc_type_zone']."', 0, 0)";
                                $result = mysqli_query($con, $query);
                                if(!$result){
                                    echo mysqli_error($con);
                                }

                                $zone_number_in = $zone_in;
                                $zone_number_out = $zone_out;
                                //echo "".$current_num." ".$zone_number." <br><br>";
                                //}
                                break;
                        }
                        close_db_connection($con);
                    }
                }
            }
        }
        //if(!$data) break;
        //$recv_len += strlen($data);
        //$response .= $data; //echo 'chunck '.$iter.' : '.$response.'<br>'; //echo 'len: '.$recv_len.'<br>';
        //if($iter>$GLOBALS['READING_ITER']) break;
        //$iter++;
    }
    socket_close($client);

    $response = bin2hex($response);//converto la stringa in esadecimale
    $hex_string = rtrim(chunk_split($response, 2, ' '), " "); //spazio ogni due caratteri e levo l'ultimo spazio

    if($hex_string != "") {

        $response = cleanCharacter($hex_string);

        if(sizeof($response)>1) {
            $response = json_decode($response, true);
            $res[IND_PACCHETTO] = $response;
        }else{
            $res[IND_INFOERRORE] = $response;
            $res[IND_ERRORE] = ERRORE_LETTURA_MESSAGGIO;
            $res[IND_PACCHETTO]=null;
        }
    }else{
        $res[IND_ERRORE] = ERRORE_MESSAGGIO_VUOTO;
        $res[IND_PACCHETTO]=null;
    }

    return $res;

}

function cleanCharacter($response){
    $response = bin2hex($response);//converto la stringa in esadecimale
    $hex_string = rtrim(chunk_split($response, 2, ' '), " "); //spazio ogni due caratteri e levo l'ultimo spazio

    $hex_arr = explode(' ', $hex_string);
    $hex_arr_clean = array();
    for ($i = 0; $i < sizeof($hex_arr); $i++) {
        //prendo solo i caratteri ascii
        if (hexdec($hex_arr[$i]) >= 0x20 && hexdec($hex_arr[$i]) < 0x7e) {
            $hex_arr_clean[] = $hex_arr[$i];
        }
    }
    //mi ricreo la stringa e la riconverto in str
    $hex_str_clean = implode('', $hex_arr_clean);
    $str_clean = hex2bin($hex_str_clean);

    //fix per creare un json valido dalla stringa
    $response = explode('{"', $str_clean, 2);
    if(sizeof($response)>1) {
        $response = '[{"' . $response[1] . ']';
        $response = str_replace('}{', '},{', $response);
    }else{
        $response = "";
    }
    return $response;
}

function debug_log_2($str){
    if($GLOBALS['print_log']) echo $str;
}
?>


