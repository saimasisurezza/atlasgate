<?php
include_once 'globals.php';

$logged = check_login();

if($logged)
    header('Location:' . $GLOBALS['home_page']);


?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"><head>

<title><?php echo $GLOBALS['APP_NAME'];?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

    <link href="library/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">

    <link href="css/general.css" rel="stylesheet" type="text/css">
    <link href="css/index.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="font/fontawesome-5.6.3/css/all.css">

    <script src="library/jquery-3.1.1.min.js"></script>

    <script src="library/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="library/bootstrap/bootstrap-filestyle.min.js"> </script>

    <script type="text/javascript" src="js/header.js"></script>

    <script type="text/javascript">

        $(document).ready(function(e) {
            printLog =  ($('body').attr('data-log') == 'true');//ottengo il booleano dalla stringa

            header = new $.header();
            header.init();

            $('#login-button').click();

        });

    </script>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
</head>

<body data-page="login" data-log="<?php echo $GLOBALS['js_log'];?>">

<?php include_once 'header.php'; ?>
    <div id="main-container">

    </div>
<?php include_once "footer.php"; ?>
</body>
</html>
