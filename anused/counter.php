<?php
include_once 'globals.php';
$consolle = true
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"><head>

    <title><?php echo $GLOBALS['APP_NAME']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

    <link rel="stylesheet" href="font/fontawesome-5.6.3/css/all.css">
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/counter.css">

    <script src="library/jquery-3.1.1.min.js"></script>
    <script src="js/header.min.js"></script>
    <script src="js/counter.js"></script>

    <script type="text/javascript">

        $(document).ready(function(e) {
            var printLog =  ($('body').attr('data-log') == 'true');//ottengo il booleano dalla stringa

            header = new $.header();
            header.init(printLog);

            counter = new $.counter();
            counter.init(printLog);

        });

    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="apple-touch-icon" sizes="180x180" href="img/icon/apple-touch-icon.png">

    <link rel="icon" type="image/png" sizes="32x32" href="img/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icon/favicon-16x16.png">
    <link rel="manifest" href="img/icon/site.webmanifest">
    <link rel="mask-icon" href="img/icon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>

<body data-page="index" data-log="<?php echo $GLOBALS['js_log'];?>">
<?php include_once 'header.php'; ?>

<div id="main-container">

    <div id="container">
        <div class="squareinfo"><div class="textinfo"><?php echo getTextT(121);?></div><div id="currentPerson" class="numinfo"></div></div><div class="squareinfo"><div class="textinfo"><?php echo getTextT(120);?></div><div id="maxPerson" class="numinfo"></div></div>
    </div>
</div>
<?php include_once "footer.php"; ?>
</body>
</html>

