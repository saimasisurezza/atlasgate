<?php

// API URL
$url = "http://localhost:8081/api/tags?pageNumber=0&pageSize=5";

// Create a new cURL resource
$ch = curl_init($url);

curl_setopt($ch, CURLOPT_GET, 1);

// Return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the POST request
$result = curl_exec($ch);
$result = json_decode($result, true);

if(array_key_exists('error', $result)){
    echo "STATUS : ".$result['status']."; ERROR : ".$result['error'].";<br>".$url;
}
else{
    var_dump($result);
}



// Close cURL resource
curl_close($ch);    