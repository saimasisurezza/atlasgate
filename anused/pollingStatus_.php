<?php
include_once dirname(__FILE__)."/../globals.php";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//TEMPI 
$GLOBALS['association_timeout'] = 5; //tempo in secondi che deve passare affinchè ci sia associazione tra PC e BLE 
$GLOBALS['max_wait_time'] = 10; //tempo massimo in secondi che un evento PC può restare in attesa di associazione
$GLOBALS['min_check_time'] = 2; //tempo minimo in secondi di attesa tra un controllo e l'altro

//DEBUG
$GLOBALS['print_status'] = false;
//DEBUG

if(file_exists($GLOBALS['lock_file'])) unlink($GLOBALS['lock_file']);

//VARIABILI
$output = array();
$output['associazioni'] = array();
$stato = CONTROLLO;
$accessPc = array();
$accessBle = array();

$output['error'] = 0;
$output['errorstring'] = null;

$con = open_db_connection();

$timePast = date('Y-m-d H:i:s', strtotime('-40 minutes'));
$timePast = strtotime($timePast);

$timeCurrentPc = date('Y-m-d H:i:s', strtotime('-10 seconds'));
$timeCurrentPc = strtotime($timeCurrentPc);

//$timeCurrentBle = date('Y-m-d H:i:s', strtotime('-5 seconds'));
//$timeCurrentBle = strtotime($timeCurrentBle);

$query = "SELECT * FROM peoplecounter WHERE timestamp > $timePast AND timestamp < $timeCurrentPc AND compared=0 ORDER BY timestamp ASC";
$result = mysqli_query($con, $query);

//$output['queryALLPC'] = $query;

if(mysqli_error($con)){
    $output['error'] = 1;
    $output['errorstring'] = mysqli_error($con);
    die(json_encode($output));
}

$index = 0;
while($row = mysqli_fetch_assoc($result)){
    $accessPc[$index] = $row;
    $index++;
}
mysqli_free_result($result);

$query = "SELECT * FROM ble WHERE timestamp > $timePast AND compared=0 ORDER BY timestamp ASC";
$result = mysqli_query($con, $query);

//$output['queryALLBLE'] = $query;

if(mysqli_error($con)){
    $output['error'] = 1;
    $output['errorstring'] = mysqli_error($con);
    die(json_encode($output));
}

$index = 0;
while($row = mysqli_fetch_assoc($result)){
    $accessBle[$index] = $row;
    $index++;
}
mysqli_free_result($result);

$alarm = false;
$noBleFound = true;
$iterFound = 0;
$indexBle = null;
$indexPc = null;
$log = null;
$exit = false;

$numPc = sizeof($accessPc);
$numBle = sizeof($accessBle);
$queryPc = null;
$queryBle = null;

if($numPc != 0) {

    for ($i = 0; $i<$numPc; $i++) {
        //$accessPc[$i]['ora'] = date('Y-m-d H:i:s', $accessPc[$i]['timestamp']);

        if ($numBle != 0) {
            for ($j = 0; $j<$numBle; $j++) {
/*
                if(empty( $accessBle[$j])) {
                    echo "---EMPTY VALUE index ".$j."---";
                    var_dump($accessBle);
                    // non ci va un continue qui??? non ha senso andare avanti se la variabile è vuota
                }

                if(empty( $accessPc[$i])) {
                    echo "---EMPTY VALUE index ".$i."---";
                    var_dump($accessPc);
                    // non ci va un continue qui??? non ha senso andare avanti se la variabile è vuota
                }
*/
                $diffTime = abs($accessPc[$i]['timestamp'] - $accessBle[$j]['timestamp']);
                //$accessBle[$j]['ora'] = date('Y-m-d H:i:s', $accessBle[$j]['timestamp']);

                $log .= " inner  sec ".$diffTime." - size pc:".$numPc." size ble:".$numBle." ";
                $log .= $GLOBALS['compareTime'];
                
                if($diffTime <= $GLOBALS['compareTime'] && $accessBle[$j]['compared'] == 0 && $accessBle[$j]['direction'] == $accessPc[$i]['direction']) {

                    $indexPc = $i;
                    $indexBle = $j;

                    $log .= " idpc ".$accessPc[$i]['id']." ";
                    $log .= "idBle".$accessBle[$j]['id']." - ";

                    $noBleFound = false;
                    $iterFound++;
                    $idPc = $accessPc[$i]['id'];
                    $idBle = $accessBle[$j]['id'];

                    $query = "UPDATE peoplecounter SET compared=1 WHERE id=$idPc";
                    $result = mysqli_query($con, $query);

                    $queryPc = $query;

                    if(mysqli_error($con)){
                       $output['error'] = 1;
                       $output['errorstring'] = mysqli_error($con);
                       die(json_encode($output));
                    }

                    $query = "UPDATE ble SET compared=1 WHERE id=$idBle";
                    $result = mysqli_query($con, $query);

                    $queryBle = $query;

                    if(mysqli_error($con)){
                       $output['error'] = 1;
                       $output['errorstring'] = mysqli_error($con);
                       die(json_encode($output));
                    }
                    $exit = true;
                    break;
                }
                $log .= " ---- ";

            }//fine for su ble

            if($noBleFound) {
                $indexPc = $i;
                $alarm = true;
                $idPc = $accessPc[$i]['id'];
                $query = "UPDATE peoplecounter SET compared=1 WHERE id=$idPc";
                $result = mysqli_query($con, $query);
                $exit = true;
                break;
            }
        }else{
            
            $alarm = true;
            $indexPc = $i;
            $idPc = $accessPc[$i]['id'];
            $query = "UPDATE peoplecounter SET compared=1 WHERE id=$idPc";
            $result = mysqli_query($con, $query);
            $exit = true;
            break;
        }

        if($exit) break;
    }

}

$output['log'] = $log;
$output['queryPc'] = $queryPc;
$output['queryBle'] = $queryBle;
$output['exit'] = $exit;
$output['indexBle'] = $indexBle;
$output['indexPc'] = $indexPc;
$output['noBleFound'] = $noBleFound;
$output['iterFound'] = $iterFound;
$output['alarm'] = $alarm;
$output['pc'] = $accessPc;
$output['ble'] = $accessBle;

close_db_connection($con);

echo json_encode($output);


