(function ($) {
    $.main = function () {
        //dichiarazione variabili alle quali si accede con _self.
        //this.isEmergency = false;
        this.BUTTON_PRESSED = 0x01;
        this.MOVEMENT = 0x04;
        this.tag = [];
        this.iterError = 0;
        this.numMaxError = 5;

        this.pollingTime = 1000;
    };

    $.main.prototype = {
        init: function () {
            var _self = this;
            /*
          _self.interval = setInterval(() => _self.checkTagStatus() , 10000);
          setTimeout(function() {
              _self.pollingStato();
          },1000);
          */
           // _self.getId();
            //self.checkTagStatus();
            //_self.interval = setInterval(() => _self.checkTagStatus() , _self.pollingTimer);

            //_self.checkTagStatus();

            _self.pollingStatus();
        },


        checkTagStatus: function(){
            var _self = this;
            //console.log('checkMessageStarted');
            $.ajax({
                type: "GET",
                url: './service/pollingBle.php',
                dataType: "json",
                //data: {cmd: cmd},
                success: function (response) {
                    console.log(response);

                    for(var l=0; l<_self.tag.length; l++){
                        _self.tag[l].found = false;
                    }

                    if(response.result == "success") {
                        $('#erroreBlue').remove();
                        _self.iterError = 0;

                        console.log("Tag rilevati: "+response.beacons.length);

                        for (var i = 0; i < response.beacons.length; i++) {

                            for(var k=0; k<_self.tag.length; k++){
                                if(response.beacons[i].serial == _self.tag[k].id) {
                                    _self.tag[k].found = true;
                                }
                            }

                            var maxIndex = 0;
                            var ultimoTs = new Date(response.beacons[i].ibeacon[maxIndex].timestamp);
                          //  console.log("lunghezza ibeacon "+response.beacons[i].ibeacon.length+" ovvero \"eventi\" per ogni tamper");

                            for(var j=1; j<response.beacons[i].ibeacon.length; j++) {
                                var attualeTs=  new Date(response.beacons[i].ibeacon[j].timestamp);
                                //console.log(attualeTs+' vs '+ultimoTs);
                                //console.log("ultimo: "+response.beacons[i].ibeacon[maxIndex].timestamp+" ||| attuale: "+response.beacons[i].ibeacon[j].timestamp);
                                if(ultimoTs.getTime() < attualeTs.getTime()) {
                                    //console.log("switch");
                                    ultimoTs = attualeTs;
                                    maxIndex = j;
                                }else{
                                //    console.log("il campione sono sempre io");
                                }
                            }
                            //console.log('scelto '+ maxIndex);

                            var currentMinor = response.beacons[i].ibeacon[maxIndex].minor;
                            var batteria = currentMinor >> 8;
                            batteria = (batteria*10) + 1700;
                            batteria = Math.ceil((batteria*100)/3000);
                            var flags = currentMinor & 0xff;

                            if(response.beacons[i].serial == 7089) {
                                //console.log(response.beacons[i]);
                                //console.log(batteria);
                            }

                            _self.createSquare(flags & _self.BUTTON_PRESSED, flags & _self.MOVEMENT, batteria, response.beacons[i].serial);
                        }

                        for(var ind=0; ind<_self.tag.length; ind++){
                          if(!_self.tag[ind].found){
                              console.log("TAG "+_self.tag[ind]+" SCOMPARSO");
                              $('.square[data-id="'+_self.tag[ind].id+'"]').addClass('error');
                          }else{
                              $('.square[data-id="'+_self.tag[ind].id+'"]').removeClass('error');
                          }
                        }
                    }
                },
                error: function (error) {
                    if(_self.iterError > _self.numMaxError) {
                        if ($('#erroreBlue').length == 0)
                            $('#main-container').prepend('<div id="erroreBlue">Gateway Bluetooth disconnesso</div>');
                        _self.iterError = 0;
                    }else{
                        _self.iterError++;
                    }
                    //console.log(error);
                }
            });
        },

        pollingStatus: function (){
            var _self = this;

            $.ajax({
                type: "POST",
                url: './service/pollingStatus.php',
                dataType: "json",
                data: {
                    device_id:''
                },
                success: function (response) {
                    console.log(response);

                    if (response.errore) {
                        console.log('error on connection '+response.errore);
                        //gestione dell'icona connected/disconnected
                        /*
                        if (!$('.square').hasClass('hide') && $('.square').attr('data-device') == dev_id) {
                            _self.iterConnectionSquareOpen++;
                            if (_self.iterConnectionSquareOpen > _self.maxConnectionError) {
                                $('#unlink').removeClass('hide');
                                $('#link').addClass('hide');
                            }
                        }

                        //questo if da il messaggio che una macchina è disconnessa dopo un numero di tentativi non riusciti
                        if (arrayConnectionError[indice] < _self.maxConnectionError) {
                            arrayConnectionError[indice]++;
                        } else {
                            //if(_self._self.arrayStatusMachine.length < _self.devices.length )
                            _self.insertAlert('error', 'Connection error to "' + device_name+'"', dev_id, $.main.defaults.errDevice);

                            if (_self.arrayConnectionError[indice] >= _self.maxConnectionError)
                                _self.arrayConnectionError[indice] = 0;
                        }
                        */
                        return;
                    }

                    if(response.indexPc != null ) {
                        var el;
                        console.log(response.pc[response.indexPc]);
                        console.log(response.pc[response.indexBle]);
                        
                        if (response.alarm) {
                            var elPc = response.pc[response.indexPc];   
                            var date = new Date(elPc['date']* 1000);


                            console.log(date.toLocaleString());
                            el = "<li class='event error'>ALLARME > Transito "+elPc['direction']+" pc:" + elPc['id'] + " data:" + date.toLocaleString() + "</li>";
                        } else {
                            var elPc = response.pc[response.indexPc];
                            var elBle = response.ble[response.indexBle];
                            var PC_date = _self.convertTimestamp(response.pc[response.indexPc]['timestamp']);
                            el = "<li class='event "+elPc['direction']+"'>Transito "+elPc['direction']+" OK pc:" + elPc['id'] + " data:" + PC_date + " dir:"+elPc['direction']+" <br>- ble:" + elBle['serial'] + " id:" + elBle['id'] + " data:" + elBle['date'] + " dir:"+elBle['direction']+"</li>";
                        }
                        $('#listCont').append(el);
                        $('body').scrollTop($('#listCont').height());

                    }
                    /*
                    if (!$('.square').hasClass('hide') && $('.square').attr('data-device') == dev_id) {
                        $('#link').removeClass('hide');
                        $('#unlink').addClass('hide');
                        arrayConnectionError[i] = 0;
                    }
                    */

                },
                error: function (error) {
                    console.log(error);
                }
            });

            setTimeout(function(){
                _self.pollingStatus()
            }, _self.pollingTime);
        },

        convertTimestamp: function(UNIX_timestamp){
            var a = new Date(UNIX_timestamp * 1000);
            var year = a.getFullYear();
            var month = a.getMonth();
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec ;
            return time;
        },

        sleep: function(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e9; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                    break;
                }
            }
        },

        /*
        createSquare:function(buttonPressed, movement, batteria, serial){
            if(serial === undefined) return;

            var square = "<div class='square' data-id='"+serial+"'><div class='squaretitle'>";
            square += "ID "+serial;
            square += "</div>";

            //button pressed
            square += "<div class='led-cont buttonPressed'>";
            if(buttonPressed)
                square += "<div class='led off'>";
            else
                square += "<div class='led on'>";

            square += "</div><div class='ledtext'>Tamper</div></div>";

            //movement
            square += "<div class='led-cont movement'>";


            if(movement)
                square += "<div class='led on'>";
            else
                square += "<div class='led off'>";

            square += "</div><div class='ledtext'>Moving</div></div>";

            var batteryDiv = "<div class='battery-cont'>";
            if(batteria>90){
                batteryDiv += "<i class='fas fa-battery-full'></i>";
            }else if(batteria>65){
                batteryDiv += "<i class='fas fa-battery-three-quarters'></i>";
            }else if(batteria>40){
                batteryDiv += "<i class='fas fa-battery-half'></i>";
            }else if(batteria>15){
                batteryDiv += "<i class='fas fa-battery-quarter'></i>";
            }else{
                batteryDiv += "<i class='fas fa-battery-empty'></i>";
            }

            batteryDiv += "<div class=batteryInfo>"+batteria+"%</div></div>";
            square += batteryDiv;

            if($('.square[data-id="'+serial+'"]').length == 0) {
                $('#main-container').append(square);
            }else{
                if(buttonPressed){
                    $('.square[data-id="'+serial+'"] .buttonPressed .led').removeClass('on').addClass('off');
                }else{
                    $('.square[data-id="'+serial+'"] .buttonPressed .led').removeClass('off').addClass('on');
                }

                if(movement){
                    $('.square[data-id="'+serial+'"] .movement .led').removeClass('off').addClass('on');
                }else{
                    $('.square[data-id="'+serial+'"] .movement .led').removeClass('on').addClass('off');
                }

                $('.square[data-id="'+serial+'"] .battery-cont').remove();

                $('.square[data-id="'+serial+'"]').append(batteryDiv);
            }
        },

        getId:function(){
            var _self = this;
            var t = new Object();
            t.id = 7080;
            t.found = false;
            _self.tag[0] = t;
            var t = new Object();
            t.id = 7081;
            t.found = false;
            _self.tag[1] = t;
            var t = new Object();
            t.id = 7082;
            t.found = false;
            _self.tag[2] = t;
            var t = new Object();
            t.id = 7083;
            t.found = false;
            _self.tag[3] = t;
            var t = new Object();
            t.id = 7084;
            t.found = false;
            _self.tag[4] = t;
            var t = new Object();
            t.id = 7085;
            t.found = false;
            _self.tag[5] = t;
            var t = new Object();
            t.id = 7086;
            t.found = false;
            _self.tag[6] = t;
            var t = new Object();
            t.id = 7087;
            t.found = false;
            _self.tag[7] = t;
            var t = new Object();
            t.id = 7088;
            t.found = false;
            _self.tag[8] = t;
            var t = new Object();
            t.id = 7089;
            t.found = false;
            _self.tag[9] = t;
            console.log(_self.tag);

            for(var ind=0; ind<_self.tag.length; ind++){
                _self.createSquare(0, 0, 0, _self.tag[ind].id);
            }

        }





                centermodal:function(){
                        $('.modal').each(function(){
                            var leftoffset= ( ($(window).width() - $(this).width() ) /2 );
                            var topoffset= (  ($(window).height() - $(this).height()) /2.3 );
                            $(this).css('left', leftoffset+'px');
                            $(this).css('top', topoffset+'px');
                        });
                    },
                    openModal: function(obj){
                        $(obj).removeClass('close').addClass('open');
                        $('.overlay').removeClass('hide').addClass('show');
                    },

                    closeModal: function(obj){
                        $(obj).removeClass('open').addClass('close');
                        $('.overlay').removeClass('show').addClass('hide');
                    }

                    showBtn: function(objtoshow, objtohide){
                        //handle change button
                        objtohide.addClass('hide');
                        objtoshow.removeClass('hide');
                    },

                    showCompleted: function(obj, downloading){
                        var _self = this;
                        obj.addClass('completed');
                        obj.text('');
                        obj.append('Completed');
                        _self.showBtn(obj, downloading);
                        downloading.text('');
                        downloading.append('Downloading 0%');
                    },

                    showBtnError: function(str, obj, objdownloading, objerror, timeout){
                        //str: message error, obj: button to show after error message, objerror: error button, timeout: flag for timeout animation
                        objerror.text(str);
                        objdownloading.addClass('hide');
                        objerror.removeClass('hide');
                        if(timeout != false){
                            setTimeout(function(){
                                objerror.addClass('hide');
                                obj.removeClass('hide');
                            },3000);
                        }
                    },
                */
 
    };

    $.main.default = {
        type: 'main'
    };

}(jQuery));
