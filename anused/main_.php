<?php
include_once dirname(__FILE__)."/../globals.php";

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//TEMPI
$GLOBALS['max_wait_time'] = 8; //massimo tempo in secondi che rimane in APRE_IN_VUOTO e in APRE_OUT_VUOTO
$GLOBALS['loitering_time'] = 6; //tempo di sosta di un utente dopo ingresso o uscita subito dopo le porte
$GLOBALS['min_check_time'] = 3; //tempo che aspetto per controllare accodamento o controflusso in APER_OUT_VUOTO e APRE_IN_VUOTO

//DEBUG
$GLOBALS['print_status'] = false;
//DEBUG

if(file_exists($GLOBALS['lock_file'])) unlink($GLOBALS['lock_file']);

$output= array();
$scriviDB=false;
$stato = QUIETE;
$command_sent = false; //mi fa mandare il comando una sola volta
$num_person_in = 0;
$num_person_out = 0;
$porta_aperta = false;
//$start_time = getTime();
//$firstTime = true;
$validatoreIngresso = 0;

//gestione tempo di reset nello stato APRE_IN_VOUTO
$tempo_reset_apre_in = 0;
$tempo_reset_apre_out = 0;

$apertura_uscita = false; //usata nel caso Normalmente aperto

$troppe_persone_led = false;
$iter_troppe_persone = 0;
$max_iter_troppe_persone = 10;

$max_people = false;
$max_people_isteresi = false;

$frode_isteresi = false;
$emergenza_isteresi = false;
$accesso_non_consentito = false;
$accesso_non_consentito_isteresi = false;

$con = open_db_connection();
clear_table($con, 'status');
$settings = getSettings($con);
$timestamp = getTime();
$query_ev = "INSERT INTO events (timestamp, code, portal_id, info) VALUES ($timestamp, ".EV_RESET_SISTEMA.", ".$settings['portal_id'].", '')";
mysqli_query($con, $query_ev);

clear_transits($con);
close_db_connection($con);

$prev_iter = 0;
$prev_func_mode = $settings['func_mode'];

while(true) {

    $startime = time();

    $con = open_db_connection();

    $settings = getSettings($con);
    //lettura numero persone nella zona
    $numZone = getNumZone($con);
    //lettura numero persone che hanno attraversato la linea con direzione IN o OUT
    $tmp = getNumCount($con, $prev_iter);
    $numCount = $tmp[0];
    $prev_iter = $tmp[1];
    $numInside = null;

    if($prev_func_mode != $settings['func_mode']){
        debug_log("\n cambio modalità di fuzionamento da $prev_func_mode a ".$settings['func_mode']."\n");
        $stato = QUIETE;
    }
    $prev_func_mode = $settings['func_mode'];

    if(intval($settings['is_slave'])){
        //se sono uno slave leggo il numero di persone che conta il master
        $con_master = open_db_connection_master($settings['ip_master']);
        $numInside = getNumInside($con_master);
        close_db_connection($con_master);
    }else {
        //lettura del numero di persone all'interno dell'edificio
        $numInside = getNumInside($con);
    }

    $mod_ingresso = $settings['mod_in'];
    $mod_uscita = $settings['mod_out'];
    //debug_log("\n Num In ".$numCount['in']. " Num Out ".$numCount['out']." max_in ".$numCount['max_in']." max_out ".$numCount['max_out']);

    //lettura stato azionamento
    $cmd = $GLOBALS['path_c'].'status';

    if($settings['model'] == PR449) $cmd .= "449";

    if ($numInside < $settings['max_people'] || $settings['max_people'] == 0) $max_people = false;
    else $max_people = true;

    if(($settings['max_people_red'] && $max_people) || $stato == APRE_IN_VUOTO || $stato == APRE_OUT_VUOTO || $troppe_persone_led){
        $cmd .= ' -r';
        if($troppe_persone_led) $iter_troppe_persone++;
    }
    $response = try_to_send($cmd);
    if(!$settings['max_people_red']){
        if($iter_troppe_persone>=$max_iter_troppe_persone){
            $troppe_persone_led = false;
            $iter_troppe_persone = 0;
        }
    }

    if(!$max_people && $max_people_isteresi){
        //debug_log("\n DISATTIVO USCITA");
        exec($GLOBALS['changeUser']."python3 ".$GLOBALS['path_python']."gpio_OUT_pin22.py 0", $tmp, $tmp1);
        $max_people_isteresi = false;
    }else if($max_people && !$max_people_isteresi){
        //debug_log("\n ATTIVO USCITA");
        exec($GLOBALS['changeUser']."python3 ".$GLOBALS['path_python']."gpio_OUT_pin22.py 1", $tmp, $tmp1);
        $max_people_isteresi = true;
    }

    //leggo lo stato del pr
    $currentTime = getTime();
    $lowerBound = $currentTime - $GLOBALS['pc_previous_time'];
    $pr_status = getStatus($con, $lowerBound);

    $event = null;
    //inserisco eventuali eventi
    if($accesso_non_consentito && !$accesso_non_consentito_isteresi){
        $event = EV_ACCESSO_NON_CONSENTITO;
        $accesso_non_consentito_isteresi = true;
    }else if(!$accesso_non_consentito && $accesso_non_consentito_isteresi){
        $accesso_non_consentito_isteresi = false;
    }
    if($pr_status['fraud'] && !$frode_isteresi && !$accesso_non_consentito){
        $event = EV_FRODE;
        $frode_isteresi = true;
    }else if(!$pr_status['fraud'] && $frode_isteresi){
        $frode_isteresi = false;
    }
    if($pr_status['emergency'] && !$emergenza_isteresi) {
        $event = EV_EMERGENZA_ON;
        $emergenza_isteresi = true;
        if($stato != EMERGENZA){
            $stato = EMERGENZA;
            debug_log("\n cambio stato: EMERGENZA\n");
        }
    }else if(!$pr_status['emergency'] && $emergenza_isteresi){
        $emergenza_isteresi = false;
        $event = EV_EMERGENZA_OFF;
        $stato = QUIETE;
        debug_log("\n cambio stato: QUIETE\n");
    }
    if($event !== null){
        $query_ev = "INSERT INTO events (timestamp, code, portal_id, info) VALUES ($currentTime, $event, ".$settings['portal_id'].", '')";
        mysqli_query($con, $query_ev);
    }

    if($settings['func_mode']==NORMALCLOSESTD){
        switch ($stato) {
            case QUIETE:
                $accesso_non_consentito = false;
                // gestione contatto di ingresso
                $return_var = array();
                $exec_response = array();
                exec($GLOBALS['changeUser'] . "python3 " . $GLOBALS['path_python'] . "contatto_apertura_ingresso.py", $exec_response, $return_var);
                $validatoreIngresso = intval($exec_response[0]);
                // gestione contatto di uscita
                $return_var = array();
                $exec_response = array();
                exec($GLOBALS['changeUser'] . "python3 " . $GLOBALS['path_python'] . "contatto_apertura_uscita.py", $exec_response, $return_var);
                $validatoreUscita = intval($exec_response[0]);

                //if (($numZone['max_in'] > 1 && $mod_ingresso == CONTROLLATO) || ($mod_ingresso != LIBERO && $numCount['max_in'] != 0 && !$validatoreIngresso)) {
                if( ($mod_ingresso == BLOCCATO && ($numCount['max_in'] > 0 || $numZone['max_in'] > 0 )) ||
                    ($mod_ingresso == CONTROLLATO && $numZone['max_in'] > 1)                            ||
                    ($mod_ingresso == CONTROLLATO && $numCount['max_in'] > 0 && !$validatoreIngresso)   ||
                    ($mod_uscita == BLOCCATO && ($numCount['max_out'] > 0 || $numZone['max_out'] > 0 )) ||
                    ($mod_uscita == CONTROLLATO && $numZone['max_out'] > 1)                             ||
                    ($mod_uscita == CONTROLLATO && $numCount['max_out'] > 0 && !$validatoreUscita)      ){
                    $stato = ALLARME;
                    $command_sent = false;
                    debug_log("\n cambio stato: ALLARME\n");
                } else if( ($mod_ingresso == LIBERO  && $numZone['max_in'] == 1) ||
                           ($mod_ingresso == CONTROLLATO && $validatoreIngresso && ($numZone['max_in'] == 1 || $GLOBALS['solo_validatore']) ) ){
                    //debug_log("\nNUM ZONE in ".$numZone['in']."  NUM ZONE out ".$numZone['out'] ."   NUM ZONE max_in ".$numZone['max_in'] ."  NUM ZONE max_out ".$numZone['max_out']);
                    if (!$max_people) {
                        $num_person_in = 0;
                        $num_person_out = 0;
                        $stato = APRE_IN;
                        $command_sent = false;
                        debug_log("\n cambio stato: APRE IN\n");
                        clear_transits($con);
                    } else {
                        debug_log("\n TROPPE PERSONE\n");
                        $troppe_persone_led = true;
                    }
                } else if( ($mod_uscita == LIBERO  && $numZone['max_out'] == 1) ||
                           ($mod_uscita == CONTROLLATO && $validatoreUscita && ($numZone['max_out'] == 1 || $GLOBALS['solo_validatore']) ) ){
                    //debug_log("\nNUM ZONE in ".$numZone['in']."  NUM ZONE out ".$numZone['out'] ."   NUM ZONE max_in ".$numZone['max_in'] ."  NUM ZONE max_out ".$numZone['max_out']);
                    $num_person_in = 0;

                    $num_person_out = 0;
                    $stato = APRE_OUT;
                    $command_sent = false;
                    debug_log("\n cambio stato: APRE OUT\n");
                    clear_transits($con);
                }

                if ($pr_status['fraud'] == 1) {
                    $stato = ALLARME;
                    $command_sent = false;
                    debug_log("\n cambio stato: ALLARME\n");
                }
                break;
            case APRE_IN:
                if (!$command_sent) {
                    $cmd = '';
                    if ($settings['model'] == PRBUS) $cmd = $GLOBALS['path_c'] . 'status -a';
                    else if ($settings['model'] == PR449) $cmd = $GLOBALS['path_c'] . 'status449 -I';

                    $response = try_to_send($cmd); //return [$exec_response, $return_var, $error]
                    if ($response[1]) $command_sent = true;
                    $porta_aperta = false;
                } else {
                    if ($pr_status['door_status'] == PORTA_APERTA) $porta_aperta = true;
                    if ($porta_aperta) {
                        $num_person_in += $numCount['max_in'];
                        debug_log("\n NUM PERSON IN: $num_person_in     max in ".$numCount['max_in']);

                        if($mod_ingresso == LIBERO){
                            if($pr_status['door_status'] == PORTA_CHIUSA) {
                                clear_transits($con);
                                $stato = QUIETE;
                                debug_log("\n cambio stato: QUIETE\n");
                                $command_sent = false;
                                break;
                            }
                        }else{
                            if ($num_person_in > 1 || $num_person_out > 0) {
                                $stato = ALLARME;
                                $accesso_non_consentito = true;
                                debug_log("\n cambio stato: ALLARME\n");
                                $command_sent = false;
                                break;
                            } else if ($pr_status['door_status'] == PORTA_CHIUSA) {
                                $stato = APRE_IN_VUOTO;
                                debug_log("\n cambio stato: APRE_IN_VUOTO con num person $num_person_in\n");
                                $command_sent = false;
                                $tempo_reset_apre_in = time();
                                break;
                            }
                        }
                    }
                }
                if ($pr_status['fraud'] == 1) {
                    $stato = ALLARME;
                    $command_sent = false;
                    debug_log("\n cambio stato: ALLARME\n");
                    break;
                }
                break;
            case APRE_IN_VUOTO:
                //entra solo se è in controllato
                $time_diff = time() - $tempo_reset_apre_in;
                debug_log("\n timediff $time_diff");

                $num_person_in += $numCount['in'];
                debug_log("\n Num person in " . $num_person_in." Zone out " . $numZone['out']);

                if($time_diff > $GLOBALS['min_check_time']) {
                    //allarme loitering
                    if( ($time_diff > $GLOBALS['loitering_time'] && $numZone['out'] > 0 && $num_person_in == 0) || $num_person_in > 1){
                        $stato = ALLARME;
                        $command_sent = false;
                        debug_log("\n cambio stato: ALLARME\n");
                        break;
                    }

                    //gestione ritorno in quiete
                    if ($time_diff > $GLOBALS['max_wait_time'] || $num_person_in == 1) {
                        clear_transits($con);
                        $stato = QUIETE;
                        debug_log("\n cambio stato: QUIETE\n");
                        $command_sent = false;
                        break;
                    }
                }

                if (($num_person_in > 1 && $mod_ingresso != LIBERO) || //controllo antiaccodamento
                    ($numCount['max_out'] > 0 && $mod_uscita != LIBERO)) { //controllo controflusso
                    $stato = ALLARME;
                    $accesso_non_consentito = true;
                    debug_log("\n cambio stato: ALLARME\n");
                    $command_sent = false;
                    clear_transits($con);
                    break;
                }

                //controllo su ulteriore ingresso con abilitazione
                $return_var = array();
                $exec_response = array();
                exec($GLOBALS['changeUser'] . "python3 " . $GLOBALS['path_python'] . "contatto_apertura_ingresso.py", $exec_response, $return_var);
                $validatoreIngresso = intval($exec_response[0]);
                if ($mod_ingresso != BLOCCATO && ($numZone['max_in'] == 1 && (($validatoreIngresso && $mod_ingresso == CONTROLLATO) || $mod_ingresso == LIBERO))) {
                    debug_log("\n cambio stato: APRE_IN\n");
                    $num_person_in = 0;
                    $stato = APRE_IN;
                    clear_transits($con);
                    break;
                }

                if ($pr_status['fraud'] == 1) {
                    $stato = ALLARME;
                    $command_sent = false;
                    debug_log("\n cambio stato: ALLARME\n");
                    break;
                }
                break;
            case APRE_OUT:
                if (!$command_sent){
                    $cmd = '';
                    if ($settings['model'] == PRBUS) $cmd = $GLOBALS['path_c'] . 'status -a';
                    else if ($settings['model'] == PR449) $cmd = $GLOBALS['path_c'] . 'status449 -U';

                    $response = try_to_send($cmd); //return [$exec_response, $return_var, $error]
                    if ($response[1]) $command_sent = true;
                    $porta_aperta = false;
                }else{
                    if ($pr_status['door_status'] == PORTA_APERTA) $porta_aperta = true;

                    if ($porta_aperta) {
                        $num_person_out += $numCount['max_out'];
                        debug_log("\n NUM PERSON OUT: $num_person_out max out ".$numCount['max_out']);

                        if($mod_uscita == LIBERO){
                            if($pr_status['door_status'] == PORTA_CHIUSA) {
                                clear_transits($con);
                                $stato = QUIETE;
                                debug_log("\n cambio stato: QUIETE\n");
                                $command_sent = false;
                                break;
                            }
                        }else {
                            if ($num_person_out > 1 || $num_person_in >0) {
                                $stato = ALLARME;
                                $accesso_non_consentito = true;
                                debug_log("\n cambio stato: ALLARME\n");
                                $command_sent = false;
                                break;
                            } else if ($pr_status['door_status'] == PORTA_CHIUSA) {
                                $stato = APRE_OUT_VUOTO;
                                debug_log("\n cambio stato: APRE_OUT_VUOTO con num person $num_person_out\n");
                                $command_sent = false;
                                $tempo_reset_apre_out = time();
                                break;
                            }
                        }
                    }
                }
                if ($pr_status['fraud'] == 1) {
                    $stato = ALLARME;
                    $command_sent = false;
                    debug_log("\n cambio stato: ALLARME\n");
                    break;
                }
                break;
            case APRE_OUT_VUOTO:
                //entra solo se è in controllato
                $time_diff = time() - $tempo_reset_apre_out;
                debug_log("\n timediff $time_diff");

                $num_person_out += $numCount['max_out'];
                debug_log("\n Num person out " . $num_person_out." Zone in " . $numZone['in']);

                if($time_diff > $GLOBALS['min_check_time'] ) {
                    //allarme loitering
                    if($time_diff > $GLOBALS['loitering_time'] && $numZone['in'] > 0 && $num_person_out == 0 ){
                        $stato = ALLARME;
                        $command_sent = false;
                        debug_log("\n cambio stato: ALLARME\n");
                        break;
                    }

                    //gestione ritorno in quiete
                    if ($time_diff > $GLOBALS['max_wait_time'] || $num_person_out == 1) {
                        clear_transits($con);
                        $stato = QUIETE;
                        debug_log("\n cambio stato: QUIETE\n");
                        $command_sent = false;
                        break;
                    }
                }

                if (($num_person_out > 1 && $mod_uscita != LIBERO) || //controllo antiaccodamento
                    ($numCount['max_in'] > 0 && $mod_ingresso != LIBERO)) { //controllo controflusso
                    $stato = ALLARME;
                    $accesso_non_consentito = true;
                    debug_log("\n cambio stato: ALLARME\n");
                    $command_sent = false;
                    clear_transits($con);
                    break;
                }

                //controllo su ulteriore ingresso con abilitazione
                $return_var = array();
                $exec_response = array();
                exec($GLOBALS['changeUser'] . "python3 " . $GLOBALS['path_python'] . "contatto_apertura_uscita.py", $exec_response, $return_var);
                $validatoreUscita = intval($exec_response[0]);
                if ($mod_uscita != BLOCCATO && ($numZone['max_out'] == 1 && (($validatoreUscita && $mod_uscita == CONTROLLATO) || $mod_uscita == LIBERO))) {
                    debug_log("\n cambio stato: APRE_OUT\n");
                    $num_person_out = 0;
                    $stato = APRE_OUT;
                    clear_transits($con);
                    break;
                }

                if ($pr_status['fraud'] == 1) {
                    $stato = ALLARME;
                    $command_sent = false;
                    debug_log("\n cambio stato: ALLARME\n");
                }
                break;
            case ALLARME:
                if (!$command_sent) {
                    $cmd = '';
                    if ($settings['model'] == PRBUS) $cmd = $GLOBALS['path_c'] . 'status -f';
                    else if ($settings['model'] == PR449) $cmd = $GLOBALS['path_c'] . 'status449 -f';

                    $response = try_to_send($cmd); //return [$exec_response, $return_var, $error]
                    if ($response[1]) $command_sent = true;
                } else if ($pr_status['fraud'] == 0 && $pr_status['door_status'] == PORTA_CHIUSA) {
                    $stato = QUIETE;
                    $command_sent = false;
                    clear_transits($con);
                    debug_log("\n cambio stato: QUIETE\n");
                }
                break;
            case EMERGENZA:
                debug_log("\n stato corrente: EMERGENZA\n");
                break;
        }
    }else if($settings['func_mode'] = NORMALOPENSTD) {
        switch ($stato) {
            case QUIETE:
                if($stato != EMERGENZA) $stato = APERTO;
                debug_log("\n cambio stato: APERTO\n");
                $cmd = '';
                if ($settings['model'] == PRBUS) $cmd = $GLOBALS['path_c'] . 'status -a';
                else if ($settings['model'] == PR449) $cmd = $GLOBALS['path_c'] . 'status449 -I';
                $response = try_to_send($cmd); //return [$exec_response, $return_var, $error]
                break;
            case APERTO:
                //ogni secondo mando il comando di apertura
                debug_log("\n stato corrente: APERTO\n");
                if (!$max_people_isteresi || $apertura_uscita) {
                    $cmd = '';
                    if ($settings['model'] == PRBUS) $cmd = $GLOBALS['path_c'] . 'status -a';
                    else if ($settings['model'] == PR449) $cmd = $GLOBALS['path_c'] . 'status449 -I';

                    $response = try_to_send($cmd); //return [$exec_response, $return_var, $error]
                    //if ($response[1]) {
                    //    $apertura_uscita = false;
                    //}else
                    if($numInside == $settings['max_people']){
                        for($k=0;$k<4;$k++){
                            $response = try_to_send($cmd);
                            usleep(500000);
                        }
                    }
                    $apertura_uscita= false;
                    sleep(1);
                } else {
                    debug_log("\n TROPPE PERSONE\n");
                    $troppe_persone_led = true;
                    $stato = CHIUSO;
                    debug_log("\n cambio stato: CHIUSO\n");
                }
                clear_transits($con);
                break;
            case CHIUSO:
                if (!$max_people_isteresi || $numZone['out'] > 0) { //transito in uscita o non ho raggiunto il numero massimo di persone allora apro
                    if($numZone['max_out']>0)
                        $apertura_uscita = true;
                    else
                        $apertura_uscita= false;

                    $troppe_persone_led = false;
                    $stato = APERTO;
                    debug_log("\n cambio stato: APERTO\n");
                    break;
                }

                if ($pr_status['fraud'] == 1 || $numCount['max_in'] > 0) {
                    $stato = ALLARME;
                    $command_sent = false;
                    debug_log("\n cambio stato: ALLARME\n");
                    break;
                }
                clear_transits($con);
                break;
            case ALLARME:
                if (!$command_sent) {
                    $cmd = '';
                    if ($settings['model'] == PRBUS) $cmd = $GLOBALS['path_c'] . 'status -f';
                    else if ($settings['model'] == PR449) $cmd = $GLOBALS['path_c'] . 'status449 -f';

                    $response = try_to_send($cmd); //return [$exec_response, $return_var, $error]
                    if ($response[1]) $command_sent = true;
                } else if( $pr_status['fraud'] == 0){
                    $command_sent = false;
                    clear_transits($con);
                    $stato = CHIUSO;
                    debug_log("\n cambio stato: CHIUSO\n");
                }

                break;
            case EMERGENZA:
                //echo "\n stato corrente: EMERGENZA\n";
                clear_transits($con);
                sleep(1);
                break;
        }
    }

    // procedura che elimina gli eventi people counter più vecchi di un tot nel db, in questo caso più vecchi di 2 ore
    //$start_time = cleanDB('pc_events', $start_time, $firstTime, $con);
    //$start_time = cleanDB('status', $start_time, $firstTime, $con);
    //$firstTime = false;
    controlMaxRows('status', $con);

    close_db_connection($con);
    usleep($GLOBALS['main_sleep']);
}

function getNumZone($con){
    //ritorna array di 2 numeri, ingresso e uscita
    $num = array();
    $num['in'] = 0; $num['out'] = 0;
    $num['max_in'] = 0; $num['max_out'] = 0;
    $currentTime = getTime();
    $lowerBound = $currentTime - $GLOBALS['pc_previous_time']; // vado dietro di tot secondi
    $query = "SELECT * FROM pc_events WHERE timestamp>$lowerBound AND type='".$GLOBALS['pc_type_zone']."' ORDER BY id ASC";
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_assoc($result)){
        //print_r($row);
        //print_r($events);
        $num['in'] = intval($row['num_zone_in']);
        $num['out'] = intval($row['num_zone_out']);
        if ($num['in'] > $num['max_in']) $num['max_in'] = $num['in'];
        if ($num['out'] > $num['max_out']) $num['max_out'] = $num['out'];
    }

    return $num;
}

function getNumCount($con, $prev_iter){
    //ritorna array di 2 numeri, ingresso e uscita
    $num = array();
    $num['in'] = 0; $num['out'] = 0;
    $num['max_in'] = 0; $num['max_out'] = 0;
    //$currentTime = getTime();
    //$lowerBound = $currentTime - $GLOBALS['pc_previous_time']; // vado dietro di tot secondi
    //$query = "SELECT * FROM pc_events WHERE timestamp>$lowerBound AND type='".$GLOBALS['pc_type_count']."' ORDER BY id ASC";
    $query = "SELECT * FROM pc_events WHERE type='".$GLOBALS['pc_type_count']."' ORDER BY id ASC";
    $result = mysqli_query($con, $query);
    while($row = mysqli_fetch_assoc($result)){
        //print_r($row);
        if($prev_iter != intval($row['id'])) {

            $num['in'] = intval($row['num_in']);
            $num['out'] = intval($row['num_out']);
            if ($num['in'] > $num['max_in']) $num['max_in'] = $num['in'];
            if ($num['out'] > $num['max_out']) $num['max_out'] = $num['out'];
        }
        $prev_iter = intval($row['id']);
    }
    return [$num, $prev_iter];
}

function getNumInside($con){
    $num = 0;
    $query = "SELECT * FROM counter ORDER BY id DESC LIMIT 1";
    $result = mysqli_query($con, $query);
    if(!$result){
        echo mysqli_error($con);
    }else {
        $last_row = mysqli_fetch_assoc($result);
        if (mysqli_num_rows($result) > 0) $num = intval($last_row['num']);
    }
    return $num;
}

function debug_log($str){
    if($GLOBALS['print_status']) echo $str;
}