<?php

// API URL
$url = "http://localhost:8081/api/publish";

// Create a new cURL resource
$ch = curl_init($url);

// Setup request to send json via POST
$data = array(
    array('portal_id'=>1,'direction'=>'in', 'unknown' => 'false')
);

$payload = json_encode($data);

curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));


//curl_setopt($ch, CURLOPT_POST, 1);

// Attach encoded JSON string to the POST fields
//curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);


// Return response instead of outputting
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute the POST request
$result = curl_exec($ch);

$result = json_decode($result, true);

if(array_key_exists('error', $result)){
    echo "STATUS : ".$result['status']."; ERROR : ".$result['error'].";<br>".$payload;
}
else{
    echo "STATUS : 200;<br>".$payload;
}

// Close cURL resource
curl_close($ch);
