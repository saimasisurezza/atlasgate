<?php
//FILES e PATH
// Different from windows 
$GLOBALS['version'] = "1.0.0";
if(PHP_OS =='WINNT'){
    $GLOBALS['site_path'] = __DIR__;
    $GLOBALS['engine_path'] = $GLOBALS['site_path'].'\engine';
    $GLOBALS['php_lib'] = $GLOBALS['engine_path'].'\functions.php';
    $GLOBALS['globals'] = '\globals.php';
}else{
    $GLOBALS['site_path'] = __DIR__.'/';
    $GLOBALS['engine_path'] = $GLOBALS['site_path']."engine/";
    $GLOBALS['php_lib'] = $GLOBALS['engine_path']."functions.php";
    $GLOBALS['globals'] = "/globals.php";
}

$GLOBALS['globals_path'] = __DIR__.$GLOBALS['globals'];
$GLOBALS['saima_t_scanner'] = false;

include_once $GLOBALS['php_lib'];

//DB settings
$GLOBALS['hostDB'] = 'localhost';
$GLOBALS['userDB'] = "root";
$GLOBALS['password'] = "opengate&"; //opengate& //root
$GLOBALS["db"] = "atlas_gate";
$GLOBALS['max_days_backup_file'] = 30; //numero di giorni passati per i quali viene mantenuto il backup
$GLOBALS['max_row'] = 1000;

//PEOPLE COUNTER FISSI
$GLOBALS['ip_pc'] = '169.254.208.150';
$GLOBALS['port_pc'] = '888';
$GLOBALS['pc_type_count'] = 'Counting';
$GLOBALS['pc_type_zone'] = 'ZoneDetection';
$GLOBALS['pc_area_in'] = "Area-in";
$GLOBALS['pc_area_out'] = "Area-out";
$GLOBALS['tagString'] = "Event";
$GLOBALS['solo_validatore'] = true; //true se deve aprire con la sola validazione, false se deve aprire con la validazione e on il people counter

//GENERAL
//$GLOBALS['host'] = 'welconconsolle.com';//192.168.0.244';
#$GLOBALS['host'] = '192.168.0.101';//getCurrentIp();
$GLOBALS['portal_id'] = 13;
$GLOBALS['host'] = 'localhost';
$GLOBALS['appName'] = "Atlas Gate";
$GLOBALS['timezone'] = "Europe/Berlin";
$GLOBALS['sessiontime'] = 15; //durata della sessione in minuti
$GLOBALS['main_sleep'] = 100000; //tempo di polling del main in microsecondi, se questo si toglie dal main poi non mi permette di aggiornare i parametri non lascia mai linero il tty.lock
$GLOBALS['pc_previous_time'] = 2;
$GLOBALS['changeUser'] = "sudo -H -u jetson ";

//API BLE
//$ipPosEngine = 'bpe-saima-demo.local'; //'169.254.208.108'; password -> bpesaima19
//$ipPosEngine = '169.254.208.108';
$ipPosEngine = 'localhost'; // password : blueup
$portPosEngine = '8080';
$ipAnchorEngine = '192.168.1.1';
$GLOBALS['apiTest'] = 'http://'.$ipPosEngine.':'.$portPosEngine.'/bpe';
$GLOBALS['apiGetEventListener'] = 'http://'.$ipPosEngine.':'.$portPosEngine.'/bpe/addEventListener';
$GLOBALS['apiGetEvents'] = 'http://'.$ipPosEngine.':'.$portPosEngine.'/bpe/getEvents';
$GLOBALS['apiGetMajor'] = 'http://'.$ipAnchorEngine.'/api/beacons';
$GLOBALS['head'] = "100";
//tag visto dal gateway rcp_vision : "TAG_N10025011-10857480"
//$GLOBALS['head'] = "TAG_N100";
//$GLOBALS['tail'] = "-10857480";

//API GATEWAY
//$ipGatEngine= 'localhost';
$ipGatEngine= '10.10.150.127';
//$portGatEngine = '8081';
$portGatEngine = '80';
$GLOBALS['api'] = 'http://'.$ipGatEngine.':'.$portGatEngine.'/api';
$GLOBALS['apiGetTags'] = 'http://'.$ipGatEngine.':'.$portGatEngine.'/api/tags?';
$GLOBALS['apiPushInfo'] = 'http://'.$ipGatEngine.'/heart/server_api/handlePortalEvents.php';
$GLOBALS['pageNumber'] = 0;
$GLOBALS['pageSize'] = 10; //pageSize mi specifica il numero di tag aspettati nella risposta alla get.
$GLOBALS['max_check_iteration'] = 1000; // dopo 1000 iterazioni io in ogni caso smetto la comunicazione con il gateway

//SERVICE
$GLOBALS['main_service'] = TRUE; //se messo a false stoppa il servizio principale
$GLOBALS['pc_service'] = TRUE; //se messo a false stoppa il servizio di pollig peoplecounter 
$GLOBALS['ble_service'] = TRUE; //se messo a false stoppa il servizio di pollig ble
$GLOBALS['tag_service'] = TRUE; //se messo a false stoppa il servizio di pollig tag


//URL
$GLOBALS['site'] = 'atlasgate/';
$GLOBALS['base_path'] = 'http://'.$GLOBALS['host'].'/'.$GLOBALS['site'];
$GLOBALS['home_page_name'] = "index.php";
$GLOBALS['home_page'] = $GLOBALS['base_path'].$GLOBALS['home_page_name'];
$GLOBALS['login_page'] = $GLOBALS['base_path']."login.php";

//LOCK FILE
$GLOBALS['lock_file'] = $GLOBALS['site_path'].'tty.lock';
$GLOBALS['max_lock_iter'] = 80;
$GLOBALS['polling_lock_file'] = 100000;

//DEBUG
$GLOBALS['js_log'] = 'true';

$GLOBALS['TIMEOUT_POLLING'] = 200000; //valore in riferimento a microsecondi
$GLOBALS['TIMEOUT_SOCKET_CONNECTION'] = 0.8; //questo valore è in riferimento ai secondi,
$GLOBALS['SOCKET_CONNECTION_OK_WINDOWS_10'] = 10056;
// https://support.microsoft.com/en-my/help/819124/windows-sockets-error-codes-values-and-meanings
$GLOBALS['SOCKET_CONNECTION_OK'] = $GLOBALS['SOCKET_CONNECTION_OK_WINDOWS_10'];
$GLOBALS['MAX_PACK_LENGTH'] = 10000;//massima lunghezza del pacchetto ricevuto
$GLOBALS['READING_ITER'] = 30; //numero di iterazioni per provare la lettura del messaggio

//SOCKET
//$GLOBALS['MAX_PACK_LENGTH'] = 1024;

$GLOBALS['pollingBle'] = 1; //1 secondi
$GLOBALS['pollingPc'] = 0; //1 secondo
$GLOBALS['pollingTags'] = 1; //12; //43200; //12 ore (43200 secondi)
$GLOBALS['fail_wait_time'] = 300; // 300 secondi = 5 minuti

$GLOBALS['queue_size'] = 5;
$GLOBALS['print_log'] = FALSE;
$GLOBALS['debug'] = TRUE;
$GLOBALS['max_log_events'] = 500;

$settings = getSettingsFromDb();
if($settings!=null){
    $GLOBALS['ip_pc'] = $settings['ip_pc'];
    $GLOBALS['portal_id'] = $settings['portal_id'];
    $GLOBALS['apiGetMajor'] = 'http://'.$settings['ip_anchor'].'/api/beacons';
}
?>
