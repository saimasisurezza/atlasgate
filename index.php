<?php include_once 'globals.php';?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"><head>

    <title><?php echo $GLOBALS['appName']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=no">

    <link rel="stylesheet" href="library/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="font/fontawesome-5.6.3/css/all.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="library/jquery-3.1.1.min.js"></script>
    <script src="library/bootstrap/js/bootstrap.js"></script>
    <script src="library/bootstrap/bootstrap-filestyle.min.js"></script>
    <script src="js/main.js"></script>

    <script type="text/javascript">

        $(document).ready(function(e) {
            var main = new $.main();
            main.init();
        });

    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="icon" href="data:;base64,="> <!--è quella che si vede vicino alla url deve essere una 16x16 con estensione .ico -->

</head>

<body>

<div id="overlay" class="hide"></div>

<div id="header">
    <div id="title"><?php echo $GLOBALS['appName']; ?><img src="img/pos2.png" ></div>
</div>

<div id="main-container">
    <ul id="listCont"></ul>

</div>

<div id="footer">
    <div id="footertext"><img src="img/logo.png" style="width: 100px;margin-right: 30px;"> 2019 © SAIMA Sicurezza S.p.a.</div>
</div>

</body>
</html>
