<?php

if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$GLOBALS['refresh_timeout'] = 2; //tempo che indica ogniqualvolta si deve chiedere al gateway nuovi dati 

//VARIABILI
$pageNumber = $GLOBALS['pageNumber']; 
$stop_scanning_tag = FALSE; // finchè non ho controllato tutti i tag non va a true
$timer = time(); // tempo di attesa tra aggiornamento e altro 
$check = TRUE; // dice se poter fare il controllo o meno
$current_iteration = 0; // dice il numero di itearzioni corrente
while($GLOBALS['tag_service']){

    if($check){
        if ($GLOBALS['print_log']) echo "\nStart update Tag association in date : ".date("Y-m-d H:i:s", time())."\n";
        //connect to database 
        $con = open_db_connection();

        // metto check a false 
        $check = FALSE;

        //controllo se la connesione con il db è andata a buon fine
        if (!check_connection($con, $output)){
            $GLOBALS['main_service'] = FALSE;
            $GLOBALS['pc_service'] = FALSE;
            $GLOBALS['ble_service'] = FALSE;
            if ($GLOBALS['print_log']) json_encode($output);
        }   

        // inizia ciclo più esterno per trovare tutti i tag censiti dal gateway
        // ed aggiornare le informazioni nel database locali
        while(!$stop_scanning_tag){

            //aggiorno le current iteration
            $current_iteration ++;

            // controllo se ho raggiunto il numero massimo di iterazioni 
            if($current_iteration >= $GLOBALS['max_check_iteration']){
                $stop_scanning_tag = TRUE;
                continue;
            }
            // prendo i tag in base al pageNumber e pageSize
            $tags = getTagsInfo($pageNumber, $GLOBALS['pageSize'], $GLOBALS['print_log']);
            
            // se $tags è null allora non ho avuto risposta dal server, a questo punto 
            // io farò altre richieste, aspettando un preiodo di tempo  
            if (is_null($tags)){
                usleep($GLOBALS['fail_wait_time']);
                continue;
            }


            // se il numero dei tag restituiti è > 0 allora significa che ho ancora dei tag da controllare 
            if(sizeof($tags) > 0){
                
                for($i = 0; $i < sizeof($tags); $i++){

                    // cerco nel database se il serial_id è presente o meno. 
                    $query = "SELECT id, name, surname FROM association WHERE serial_id LIKE'".$tags[$i]["serialId"]."'";
                    $output = db_select_query($con, $query);
                    
                    // se il serial_id è gia presente nel database allora io controllo nome e cognome
                    // se sono uguali a quelli presi dal gataway allora lascio stare altrimenti li modifico
                    if (sizeof($output) > 0){
                        // controllo nome e cognome
                        
                        //caso 1 diverso solo il nome
                        if($output[0]['name'] != $tags[$i]['name'] and $output[0]['surname'] == $tags[$i]['surname']){
                            $query = "UPDATE association SET name = '".$tags[$i]["name"]."' WHERE id =".$output[0]['id'];
                            $result = db_insert_query($con, $query);
                            check_query($con, $query, $GLOBALS['print_log']);
                        }
                        
                        //caso 2 diverso solo il cognome
                        if($output[0]['name'] == $tags[$i]['name'] and $output[0]['surname'] != $tags[$i]['surname']){
                            $query = "UPDATE association SET surname = '".$tags[$i]["surname"]."' WHERE id =".$output[0]['id'];
                            $result = db_insert_query($con, $query);
                            check_query($con, $query, $GLOBALS['print_log']);
                        }
                        
                        //caso 3 diversi entrambi
                        if($output[0]['name'] != $tags[$i]['name'] and $output[0]['surname'] != $tags[$i]['surname']){
                            $query = "UPDATE association SET name = '".$tags[$i]["name"]."', surname = '".$tags[$i]["surname"]."' WHERE id =".$output[0]['id'];
                            $result = db_insert_query($con, $query);
                            check_query($con, $query, $GLOBALS['print_log']);
                        }
                    }else{
                        // se è zero allora inserisco il nuovo elemento
                        $query = "INSERT INTO association (entity_id, serial_id, name, surname) VALUES (".$tags[$i]["entityId"].", '".$tags[$i]["serialId"]."', '".$tags[$i]["name"]."', '".$tags[$i]["surname"]."')";
                        $result = db_insert_query($con, $query);
                        check_query($con, $query, $GLOBALS['print_log']);
                    }
                                        
                }

                // aggiorno il numero dei tag totali
                $pageNumber ++;

                // faccio un controllo ulteriore: controllo che il numero dei tag 
                // restituiti siano = al pageSize. Se sono minori significa che alla
                // prossima richiesta non otterrò più tag, dato che pageSize mi specifica
                // il numero di tag aspettati nella risposta alla get.
                if(sizeof($tags) < $GLOBALS['pageSize']){
                    $stop_scanning_tag = TRUE;
                }

            }   
            // se il numero dei tag non è maggiore di zero ho finito di controllare 
            else{
                $stop_scanning_tag = TRUE;
            }

        }
        
        //chiudo la connessione
        close_db_connection($con);

    }
   

    // faccio uno sleep di tot ore, l'aggiornamento è lento dato che, idealmente,
    // i tag vengono associati a delle persone che permangono nella struttura per un 
    // periodo di tempo maggiore di un giorno. (ogni docici ore aggiorno)
    if (date("H", time()) - date("H", $timer) >= $GLOBALS['pollingTags']){ //absolute?
        $timer = time();
        $check = TRUE;
    }
    
    // per il prossimo ciclo inizializzo nuovamente i due parametri della GET
    $pageNumber = $GLOBALS['pageNumber'];
    $current_iteration = 0;

}