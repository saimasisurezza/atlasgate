<?php 

if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//VARIABILI
$output = array();
$output['error'] = 0;
$output['errorstring'] = null;

$con = open_db_connection();

$query = "SELECT * FROM log WHERE checked=0 ORDER BY timestamp ASC";

$result = mysqli_query($con, $query);

if(mysqli_error($con)){
    $output['error'] = 1;
    $output['errorstring'] = mysqli_error($con);
    die(json_encode($output));
}

$noIterFound = true;
$output['associazioni'] = array();

if (!$result) {
    $output['error'] = 2;
    $output['errorstring'] = mysqli_error($result);
}
else{
    $index = 0;
    while($row = mysqli_fetch_assoc($result)){
        $output['associazioni'][$index] = $row;
        $index ++;
    }
    if($index>0) $noIterFound = false;
}   



for($i=0; $i<sizeof($output['associazioni']); $i++){
    $query = "UPDATE log SET checked=1 WHERE id=".$output['associazioni'][$i]['id'];
    $result = mysqli_query($con, $query);
    if(mysqli_error($con)){
        $output['error_update'] = 1;
        $output['errorstring_update'] = mysqli_error($con);
        die(json_encode($output));
    }

}

$output['noIterFound'] = $noIterFound;

close_db_connection($con);
echo json_encode($output);