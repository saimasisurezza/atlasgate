<?php

if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//$res = array();
///$res[IND_PACCHETTO] = "";
//$res[IND_ERRORE] = NESSUN_ERRORE;
//$res[IND_INFOERRORE] = "";

$header = "GET /HTTP/1.1\r\n" .
    "Origin: Http : //".$GLOBALS['ip_pc']."\r\n" .
    "Authorization: Basic cm9vdDpzNGltYXNpYw==\r\n" .
    //"Sec-WebSocket-Protocol: event-protocol\r\n" .
    "Sec-WebSocket-Key: cm9vdDpzNGltYXNpYw==\r\n" .
    "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko\r\n" .
    "Host: ".$GLOBALS['ip_pc'].":".$GLOBALS['port_pc']."\r\n" .
    "Upgrade: Websocket\r\n" .
    "Sec-websocket-version: 13\r\n" .
    "DNT: 1\r\n" .
    "Cache-Control: no-cache\r\n\r\n";
$msg = "event-protocol";
$buffer = $header." ".$msg;
//$buffer = $header;

$client = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
# connect to our target host
if(!$client){
    if ($GLOBALS['print_log']) echo "\n\n".socket_strerror(socket_last_error());
    //$res[IND_ERRORE] = ERRORE_CREAZIONE_SOCKET;
    //return $res;
}

$connect_timeval = array("sec"=>0, "usec" => $GLOBALS['TIMEOUT_POLLING']);
socket_set_option($client, SOL_SOCKET, SO_RCVTIMEO, $connect_timeval);
//il timeout sulla connessione non funziona, va solo sui messaggi successivi quando la connessione è già attiva
//per avere il timeout anche sulla connessione al socket setto il socket come non bloccante aspetto il tempo di polling e lo risetto a bloccante e controllo
//se ci sono eventuali errori, se ci sono non ho connssione altrimenti la connessione c'è
//socket_set_nonblock($client);
//@socket_connect($client, $target, $port);

//la @ non mi fa stampare le eventuali warning e mi stampo solo se voglio l'erroe con socket_last_error()

// switch to non-blocking
socket_set_nonblock($client);

// store the current time
$time = microtime(true);
// loop until a connection is gained or timeout reached
while (!@socket_connect($client, $GLOBALS['ip_pc'], $GLOBALS['port_pc'])) {
    $err = socket_last_error($client);
    // success!
    if ($err === 56 || $err == 0) break;

    $diff = (microtime(true) - $time);
    $code = socket_last_error($client);
    if ($GLOBALS['print_log']) echo ' - '.$err.': '.socket_strerror($err).' | '.$code.' --- ';
    if ($diff >= $GLOBALS['TIMEOUT_SOCKET_CONNECTION']) break;
    if (intval($code) == $GLOBALS['SOCKET_CONNECTION_OK']) break;

    socket_clear_error ($client);
    // sleep for a bit
    usleep(1000);
}

// re-block the socket if needed
socket_set_block($client);

if(socket_last_error($client) != 0 && socket_last_error($client) != $GLOBALS['SOCKET_CONNECTION_OK']){
    if ($GLOBALS['print_log']) echo "\n\n".socket_strerror(socket_last_error());
    socket_close($client);
    //$res[IND_ERRORE] = ERRORE_CONNESSIONE_SOCKET;
    //return $res;
}

if(!socket_write($client, $buffer)){ //quando mando il pacchetto questo deve essere in tale formato: o con 0x00 o direttamente il numero senza lo 0 davanti
    if ($GLOBALS['print_log']) echo "\n\n".socket_strerror(socket_last_error());
    socket_close($client);
    //$res[IND_ERRORE] = ERRORE_SCRITTURA_SOCKET;
    //return $res;
}
//importante si blocca su socket_read se mi metto in ascolto e non mi arriva nulla,
//per evitare questo devo sapere la lunghezza del messaggio di risposta.
//per il messaggio di status è 17
$msg_len = $GLOBALS['MAX_PACK_LENGTH'];

$zone_number_in = -1;
$zone_number_out = -1;
$timestamp = time();
while($GLOBALS['pc_service']){
    $data = @socket_read($client, $msg_len);
    if($data) {
        $data = cleanCharacter($data);
        if($data != "") {
            $data = json_decode($data, true);
            
            if(isset($data[0]['Tag'])) {
                if ($data[0]['Tag'] == $GLOBALS['tagString']) {
                    //print_r($data);
                    $ruleType = $data[0]['Data'][0]['RuleType'];
                    $con = open_db_connection();

                    //controllo se la connesione con il db è andata a buon fine
                    if (!check_connection($con, $output)){
                        $GLOBALS['main_service'] = FALSE;
                        $GLOBALS['pc_service'] = FALSE;
                        $GLOBALS['ble_service'] = FALSE;
                        if ($GLOBALS['print_log']) json_encode($output);
                    }

                    
                    switch ($ruleType){
                        case $GLOBALS['pc_type_count']:
                            $in_num = intval($data[0]['Data'][0]['CountingInfo'][0]['In']);
                            $out_num = intval($data[0]['Data'][0]['CountingInfo'][0]['Out']);
                            //debug_log_2("\nIN: ".$in_num."  OUT: ".$out_num." \n");

                            //per ogni ingresso metto nel database un record 
                            for ($i = 0; $i < $in_num; $i++){
                                $timestamp = time();
                                $query = "INSERT INTO peoplecounter (direction, timestamp, checked) VALUES ('in', $timestamp, 0)";
                                $result = mysqli_query($con, $query);
                                check_query($con, $query, $GLOBALS['print_log']);
                            }
                            

                            //per ogni uscita metto nel database un record
                            for ($i = 0; $i < $out_num; $i++){
                                $timestamp = time();
                                $query = "INSERT INTO peoplecounter (direction, timestamp, checked) VALUES ('out', $timestamp, 0)";
                                $result = mysqli_query($con, $query);
                                check_query($con, $query, $GLOBALS['print_log']);
                            }
                            
                            break;
                    }
                    close_db_connection($con);
                }
            }
        }
    }

    //sleep
    sleep($GLOBALS['pollingPc']);
}
socket_close($client);



function debug_log_2($str){
    if($GLOBALS['print_log']) echo $str;
}

function clean($data){
    $data = str_replace(' ', '-', $data);
    return preg_replace('/[^A-Za-z0-9\-]/', '', $data);
}

function cleanCharacter($response){
    $response = bin2hex($response);//converto la stringa in esadecimale
    $hex_string = rtrim(chunk_split($response, 2, ' '), " "); //spazio ogni due caratteri e levo l'ultimo spazio

    $hex_arr = explode(' ', $hex_string);
    $hex_arr_clean = array();
    for ($i = 0; $i < sizeof($hex_arr); $i++) {
        //prendo solo i caratteri ascii
        if (hexdec($hex_arr[$i]) >= 0x20 && hexdec($hex_arr[$i]) < 0x7e) {
            $hex_arr_clean[] = $hex_arr[$i];
        }
    }
    //mi ricreo la stringa e la riconverto in str
    $hex_str_clean = implode('', $hex_arr_clean);
    $str_clean = hex2bin($hex_str_clean);
    
    //fix per creare un json valido dalla stringa
    $response = explode('{"', $str_clean, 2);
    if(sizeof($response)>1) {
        $response = '[{"' . $response[1] . ']';
        $response = str_replace('}{', '},{', $response);
    }else{
        $response = "";
    }
    return $response;
}

?>