<?php
if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//TEMPI 
$GLOBALS['association_timeout'] = 5; //tempo in secondi che deve passare affinchè ci sia associazione tra PC e BLE 
$GLOBALS['max_wait_time'] = 10; //tempo massimo in secondi che un evento PC può restare in attesa di associazione
$GLOBALS['min_check_time'] = 500000; //tempo minimo in millisecondi di attesa tra un controllo e l'altro
$GLOBALS['drop_checked_infos'] = 2; //tempo minimo in ore di attesa tra un controllo e l'altro

//DEBUG
$GLOBALS['print_status'] = false;
//DEBUG

if(file_exists($GLOBALS['lock_file'])) unlink($GLOBALS['lock_file']);

//VARIABILI
$output = array();
$accessPc = array();
$accessBle = array();
$deleting_pc = array();
$deleting_ble = array();

$output['error'] = 0;
$output['errorstring'] = null;

$timer = time();

while($GLOBALS['main_service']){
    //apro la connessione con il database
    $con = open_db_connection();

    //controllo se la connesione con il db è andata a buon fine
    if (!check_connection($con, $output)){
        $GLOBALS['main_service'] = FALSE;
        $GLOBALS['pc_service'] = FALSE;
        $GLOBALS['ble_service'] = FALSE;
        if ($GLOBALS['print_log']) json_encode($output);
    }

    //leggo eventi del pc
    $query = "SELECT id, direction, timestamp FROM peoplecounter WHERE checked = 0 ORDER BY id ASC";
    $accessPc = merge_information($accessPc, db_select_query_state($con, $query, CONTROLLO));

    //leggo eventi dal ble
    $query = "SELECT id, direction, tag_id, timestamp FROM ble WHERE checked = 0 ORDER BY id ASC";
    $accessBle = merge_information($accessBle, db_select_query_state($con, $query, CONTROLLO));

    //controllo per ogni evento del people counter se c'è stato un passaggio 
    for($i = 0; $i<sizeof($accessPc); $i++){
        switch ($accessPc[$i]['stato']){
            
            //stato zero: qui si controlla se ci sono stati eventi 
            //del ble, se si si controlla se ci sono accoppiamenti
            case CONTROLLO:
                //controllo se ho degli eventi ble 
                if (sizeof($accessBle) != 0){
                    
                    //per ogni ble con la stessa direzione controllo il timestamp, 
                    //se minore di una certa soglia allora ho associazione
                    //alla fine prendo comunque più vecchio temporalmente
                    $result = compare_events($accessPc[$i], $accessBle, $con);

                    //se ho avuto un accoppiamento aggiorno lo stato dell'evento
                    //e poi aggiungo i campi legati al ble
                    if ($result != null){

                        $accessPc[$i]['ble_id'] = $result['ble_id'];
                        $accessPc[$i]['tag_id'] = $result['tag_id'];
                        $accessPc[$i]['array_index'] = $result['array_index'];
                        $accessPc[$i]['stato'] = SEGNALAZIONE;
                    }
                }

                //controllo eventi pc: se non sono stati accoppiati guardo da quanto
                //tempo sono nella lista, se sono da tanto devo segnalare un passaggio
                //$diffTime = time() - $accessPc[$i]['timestamp'];
                $diff_in_seconds = strtotime(date("Y-m-d H:i:s", time())) - strtotime(date("Y-m-d H:i:s", $accessPc[$i]['timestamp']));
                if ($diff_in_seconds > $GLOBALS['max_wait_time'] && $accessPc[$i]['stato'] == CONTROLLO){
                    $accessPc[$i]['stato'] = SEGNALAZIONE;
                    $query = "UPDATE peoplecounter SET checked=1 WHERE id=".$accessPc[$i]['id'];
                    db_update_query($con, $query);
                }
                break;
            
            //stato uno: in questo stato ci possono essere due casi:
            // -> l'evento pc è stato associato.
            // -> l'evento pc non è stato associato.
            // questo viene controllato vedendo se nei campi del pc ci sono quelli relativi al ble
            // in ogni caso viene fatta una segnalazione e si va poi all'aggiornamento del db e lista
            case SEGNALAZIONE:
                
                if(array_key_exists('ble_id', $accessPc[$i])){
                    //segnalo un passaggio corretto
                    $tag_id = "TAG_N".$accessPc[$i]['tag_id']."-10857480";
                    $data = array(
                        "portal_id" => $GLOBALS['portal_id'],
                        "direction" => $accessPc[$i]['direction'],
                        "unknown" => TRUE,
                        "tag_id" => $tag_id
                    );
                    var_dump($data);
                    $accessPc[$i]['stato'] = AGGIORNAMENTO;
                    //TODO: scommentare líf
                    if(pushCrossingInfo($data, $GLOBALS['print_log']) && $GLOBALS['debug']){                    
                        // prendo nome e cognome del transito se ho visto un tag_ble passare 
                        $identity = take_cross_identity($con, $accessPc[$i]['tag_id']);
                        $query = "";
                        // aggiorno il database log per la visualizzazione in locale del sistema 
                        if (!array_key_exists("ERROR", $identity)){
                            $query = "INSERT INTO log (direction, name, surname, timestamp, portal_id, checked) VALUES 
                                    ('".$accessPc[$i]['direction']."', '".$identity['name']."', '".$identity['surname']."',
                                    ".$accessPc[$i]['timestamp'].", ".$GLOBALS['portal_id'].", 0)";

                        }
                        //se il tag non e'stato associato a nessuna persona allora faccio la segnalazione di un passaggio di uno scono-sciuto
                        else{
                            $query = "INSERT INTO log (direction, name, surname, timestamp, portal_id, checked) VALUES 
                                    ('".$accessPc[$i]['direction']."', 'sconosciuto', '".$accessPc[$i]['tag_id']."',
                                    ".$accessPc[$i]['timestamp'].", ".$GLOBALS['portal_id'].", 0)";
                        }

                        $last_insert_id = insert_with_returned_id($con, $query, $GLOBALS['print_log']);
                        if ($last_insert_id >= $GLOBALS['max_log_events'] && $last_insert_id != -1){
                            
                            $truncate =  "TRUNCATE TABLE log";
                            db_insert_query($con, $truncate);
                            check_query($con, $truncate, $GLOBALS['print_log']);
                            insert_with_returned_id($con, $query, $GLOBALS['print_log']);
    
                        }elseif($last_insert_id == -1 && $GLOBALS['print_log']){
                            echo "ERROR IN TRUNCATE log TABLE";
                        }
                    
                    }
                    
                }
                else{
                    //segnalo un passaggio non corretto
                    $data = array(
                        "portal_id" => $GLOBALS['portal_id'],
                        "direction" => $accessPc[$i]['direction'],
                        "unknown" => FALSE,
                        "tag_id" => ""
                    );
                    var_dump($data);

                    $accessPc[$i]['stato'] = AGGIORNAMENTO;
                    //TODO: scommentare líf
                    if(pushCrossingInfo($data, $GLOBALS['print_log']) && $GLOBALS['debug']){
                        $accessPc[$i]['stato'] = AGGIORNAMENTO;

                        // aggiorno il database log per la visualizzazione in locale del sistema 
                        $query = "INSERT INTO log (direction, timestamp, portal_id, checked) VALUES 
                                ('".$accessPc[$i]['direction']."', ".$accessPc[$i]['timestamp'].", ".$GLOBALS['portal_id'].", 0)";

                        $last_insert_id = insert_with_returned_id($con, $query, $GLOBALS['print_log']);
                        if ($last_insert_id >= $GLOBALS['max_log_events'] && $last_insert_id != -1){
                            
                            $truncate =  "TRUNCATE TABLE log";
                            db_insert_query($con, $truncate);
                            check_query($con, $truncate, $GLOBALS['print_log']);
                            insert_with_returned_id($con, $query, $GLOBALS['print_log']);

                        }elseif($last_insert_id == -1 && $GLOBALS['print_log']){
                            echo "ERROR IN TRUNCATE log TABLE";
                        }
                    }
                }
                break;
            
            //stato due: si elimina l'elemento dalla lista e si aggiorna i corrispettivi
            //campi checked nelle tabelle
            case AGGIORNAMENTO:
                
                //if(array_key_exists('ble_id', $accessPc[$i])){
                    //$query = "UPDATE ble SET checked=1 WHERE id=".$accessPc[$i]['ble_id'];
                    //db_update_query($con, $query);
                //    $deleting_ble[] = $accessPc[$i]['array_index'];
                //}

                //$query = "UPDATE peoplecounter SET checked=1 WHERE id=".$accessPc[$i]['id'];
                //db_update_query($con, $query);
                $accessPc[$i]['stato'] = AGGIORNAMENTO;

                $deleting_pc[] = $i;

                break;
            
            default:
                break;

        }
    }

    // controllo se qualche ble non è stato associato
    if(sizeof($accessBle) > 0){
        for($i = 0; $i < sizeof($accessBle); $i ++){
            $diff_in_seconds = strtotime(date("Y-m-d H:i:s", time())) - strtotime(date("Y-m-d H:i:s", $accessBle[$i]['timestamp']));
            if($accessBle[$i]['stato'] == CONTROLLO && $diff_in_seconds >= ($GLOBALS['association_timeout'] + 3)){

                // comunico a RCP_vision server il transito
                $tag_id = "TAG_N".$accessBle[$i]['tag_id']."-10857480";

                $data = array(
                    "portal_id" => $GLOBALS['portal_id'],
                    "direction" => $accessBle[$i]['direction'],
                    "unknown" => TRUE,
                    "tag_id" => $tag_id
                    /*"info" => array(
                        "ble" => $accessBle[$i]['tag_id']
                    )*/
                );
                var_dump($data);

                //TODO: gestire fallimento invio
                $info_send = pushCrossingInfo($data, $GLOBALS['print_log']);
                
                $query = "UPDATE ble SET checked=1 WHERE id=".$accessBle[$i]['id'];
                db_update_query($con, $query);

                // elimino l'elemento
                $deleting_ble[] = $i;

                if ($GLOBALS['debug']){
                    // prendo identita e aggiorno log 
                    $identity = take_cross_identity($con, $accessBle[$i]['tag_id']);
                    $query = "";
                    if (!array_key_exists("ERROR", $identity)){
                        $query = "INSERT INTO log (direction, name, surname, timestamp, portal_id, checked) VALUES 
                                ('".$accessBle[$i]['direction']."', '".$identity['name']."', '".$identity['surname']."',
                                ".$accessBle[$i]['timestamp'].", ".$GLOBALS['portal_id'].", 0)";
                    }else{
                        $query = "INSERT INTO log (direction, name, surname, timestamp, portal_id, checked) VALUES 
                                ('".$accessBle[$i]['direction']."', 'sconosciuto', '".$accessBle[$i]['tag_id']."',
                                ".$accessBle[$i]['timestamp'].", ".$GLOBALS['portal_id'].", 0)";
                    }

                    $last_insert_id = insert_with_returned_id($con, $query, $GLOBALS['print_log']);
                    if ($last_insert_id >= $GLOBALS['max_log_events'] && $last_insert_id != -1){

                        $truncate =  "TRUNCATE TABLE log";
                        db_insert_query($con, $truncate);
                        check_query($con, $truncate, $GLOBALS['print_log']);
                        insert_with_returned_id($con, $query, $GLOBALS['print_log']);
                        

                    }elseif($last_insert_id == -1 && $GLOBALS['print_log']){
                        echo "ERROR IN TRUNCATE log TABLE";
                    }

                }
            }
            elseif($accessBle[$i]['stato'] == ASSOCIATO){
                $deleting_ble[] = $i;
            }
        }
    }
    //cancello elementi analizzati pc 
    for($i = 0; $i < sizeof($deleting_pc); $i++){
        array_splice($accessPc, $deleting_pc[$i], 1);
    }
 
    //cancello elementi analizzati ble
    for($i = 0; $i < sizeof($deleting_ble); $i++){
        array_splice($accessBle, $deleting_ble[$i], 1);
    }

    $deleting_pc = array();
    $deleting_ble = array();
    
    //clear table after 2 hours
    if (date("H", time()) - date("H", $timer) >= $GLOBALS['drop_checked_infos']){
        if ($GLOBALS['print_log']) echo "\n Starting clear the tables \n";
        clear_table($con, "peoplecounter");
        clear_table($con, "ble");
        $timer = time();

    }

    //chiudo la connessione
    close_db_connection($con);
    
    //attendo prima della successiva analisi
    usleep($GLOBALS['min_check_time']);

}
