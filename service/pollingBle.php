<?php
if(PHP_OS == 'WINNT'){
    include_once dirname(__FILE__).'\..\globals.php';
}else{
    include_once dirname(__FILE__)."/../globals.php";
}

function getToken($log){
    $data = array();
    $data[0] = 'tagTransit';
    $data = json_encode($data);
    $out = postRequest($GLOBALS['apiGetEventListener'], $data);
    if ($log) var_dump($out);
    return json_decode($out)->token;
}

$token = getToken($GLOBALS['print_log']);
$timestamp = time();

while($GLOBALS['ble_service']) {
    $min = intval(date("i"));
    $sec = intval(date("s"));
    if ($min == 0 && $sec == 0){
        $token = getToken($GLOBALS['print_log']);
    }
    
    $data = array();
    $data['token'] = $token;
    $data = json_encode($data);
    $out = json_decode( postRequest($GLOBALS['apiGetEvents'], $data) );

    //var_dump($out);
    if(!empty($out)){
        //connessione al database
        $con = open_db_connection();

        //controllo se la connesione con il db è andata a buon fine
        if (!check_connection($con, $output)){
            $GLOBALS['main_service'] = FALSE;
            $GLOBALS['pc_service'] = FALSE;
            $GLOBALS['ble_service'] = FALSE;
            if ($GLOBALS['print_log']) json_encode($output);
        }

        $beacons_serial_ids = json_decode(getRequest($GLOBALS["apiGetMajor"]),true);

        for($i=0; $i<sizeof($out); $i++) {

            $direction = $out[$i]->data->direction;
            $mac_address = $out[$i]->data->tag_id;
            $tag_id = getBLESerialId($beacons_serial_ids, $mac_address, $con);
            $model = $tag_id;
            $serial = $tag_id;
            //$model = $out[$i]->data->blueup->model;
            //$serial = $out[$i]->data->blueup->serial;
            //$timestamp = $out[$i]->timestamp;
            $timestamp = time();
            $tempo = date("Y-m-d H:i:s", $timestamp);
            $query = "INSERT INTO ble (direction, tag_id, model, serial, timestamp, checked, date) VALUES ('$direction', '$tag_id', '$model', '$serial', $timestamp, 0, '$tempo')";
            $result = mysqli_query($con, $query);
            
            check_query($con, $query, $GLOBALS['print_log']);
        }

        close_db_connection($con);

    }

    sleep($GLOBALS['pollingBle']);
    
}


